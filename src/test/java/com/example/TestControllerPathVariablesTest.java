package com.example;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.annotation.test.TestData;
import http.server.server.MockHTTPServerStarter;
import http.server.util.enums.TestDataTypeEnum;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Case(url = "path/test2/3")
public class TestControllerPathVariablesTest {

    private static MockHTTPServerStarter serverStarter;

    @TestData(type = TestDataTypeEnum.Constants.SPLITABLE_STRING, name = "get")
    public static String get = "test1=something&test2=anything";

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }

    private static void checkPathParserForBaseMehtods(HttpExchange obj){
        Map<String, Object> data = new Gson().fromJson(obj.getResponseBody().toString(), Map.class);
        assertNotNull(data);
        assertEquals(2, data.size());
        assertEquals("test2", data.get("test2"));
        assertEquals("3", data.get("test3"));
    }

    @Test
    @MethodCase(
            name = "TEST 1. GET",
            description = "Simple test for GET METHOD. Check path params parser.",
            testDataNameReferences = {"get"})
    public void testGET() {
        HttpExchange obj = serverStarter.call();
        checkPathParserForBaseMehtods(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 2. POST",
            description = "Simple test for POST METHOD. Check path params parser.",
            verb = "POST")
    public void testPOST() {
        HttpExchange obj = serverStarter.call();
        checkPathParserForBaseMehtods(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 3. PUT",
            description = "Simple test for PUT METHOD. Check path params parser.",
            verb = "PUT")
    public void testPUT() {
        HttpExchange obj = serverStarter.call();
        checkPathParserForBaseMehtods(obj);
    }


    @Test
    @MethodCase(
            name = "TEST 4. DELETE",
            description = "Simple test for DELETE METHOD. Check path params parser.",
            verb = "DELETE")
    public void testDelete() {
        HttpExchange obj = serverStarter.call();
        checkPathParserForBaseMehtods(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 6. ONERROR (return void).",
            description = "Test for @CustomPoint throws Exception handling by default @OnError at controller.",
            verb = "ONERROR"
    )
    public void testCustomPointOnError() {
        HttpExchange obj = serverStarter.call();
        assertEquals("{}", obj.getResponseBody().toString());
        assertEquals(501, obj.getResponseCode());
    }

}
