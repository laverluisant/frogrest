package com.example;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.annotation.test.TestData;
import http.server.exception.CannotConvertResponseException;
import http.server.server.MockHTTPServerStarter;
import http.server.util.BaseUtils;
import http.server.util.UrlParamUtils;
import http.server.util.enums.TestDataTypeEnum;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Case
public class TestRestControllerSimpleTest {

    @TestData(type = TestDataTypeEnum.Constants.SPLITABLE_STRING, name = "get")
    public static String get = "t6=something";

    @TestData(type = TestDataTypeEnum.Constants.MAP, name = "map")
    public static HashMap<String, String> map = new HashMap<>() {{
        put("t6", "t6");
        put("t7", "7");
    }};

    @TestData(type = TestDataTypeEnum.Constants.HEADER, name = "request-header")
    public static Headers headers = new Headers() {{
        put("Accept-language", new ArrayList<>() {{
            add("en-Us");
        }});
    }};

    private static MockHTTPServerStarter serverStarter;

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }

    @Test
    @MethodCase(
            name = "TEST 1. GET return void",
            description = "Simple test for GET METHOD. Request void method.",
            url = "t/t1/t2/t3/t4")
    public void testGET() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(BaseUtils.EMPTY_STRING, obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 2. POST",
            description = "Simple test for POST METHOD. Request void method.",
            verb = "POST",
            url = "t/t1/t2/t3/t4")
    public void testPOST() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(BaseUtils.EMPTY_STRING, obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 3. PUT",
            description = "Simple test for PUT METHOD. Request void method.",
            verb = "PUT",
            url = "t/t1/t2/t3/t4")
    public void testPUT() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(BaseUtils.EMPTY_STRING, obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 4. DELETE",
            description = "Simple test for DELETE METHOD. Request void method.",
            verb = "DELETE",
            url = "t/t1/t2/t3/t4")
    public void testDelete() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(BaseUtils.EMPTY_STRING, obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 5. GET return void",
            description = "Simple test for GET METHOD @RestController.",
            url = "t/t1/t2/t3/t4/t5",
            testDataNameReferences = "get",
            testDataRequestHeader = "request-header")
    public void testGET2() throws CannotConvertResponseException {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        String[] data = BaseUtils.splitByPipeline(obj.getResponseBody().toString());
        assertEquals("t4", data[0]);
        assertEquals("t5", data[1]);
        Assert.assertEquals(UrlParamUtils.splitParamsToMap(get).get("t6"), data[2]);
        assertEquals(200, obj.getResponseCode());
        Headers rqHeaders = obj.getRequestHeaders();
        for (String name : rqHeaders.keySet()) {
            assertEquals(headers.get(name), rqHeaders.get(name));
        }
        Headers rHeaders = obj.getResponseHeaders();
        assertEquals("localhost:8765", rHeaders.get("Host").get(0));
        assertEquals("en-Us", rHeaders.get("Accept-language").get(0));
    }

    @Test
    @MethodCase(
            name = "TEST 6. POST",
            description = "Simple test for POST METHOD @RestController.",
            verb = "POST",
            url = "t/t1/t2/t3/t4/t5",
            testDataNameReferences = "map")
    public void testPOST2() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        String[] data = BaseUtils.splitByPipeline(obj.getResponseBody().toString());
        assertEquals("t3", data[0]);
        assertEquals(map.get("t6"), data[1]);
        assertEquals(map.get("t7"), data[2]);
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 7. PUT",
            description = "Simple test for PUT METHOD @RestController.",
            verb = "PUT",
            url = "t/t1/t2/t3/t4/t5",
            testDataNameReferences = "map")
    public void testPUT2() {
        HttpExchange obj = serverStarter.call();
        String[] data = BaseUtils.splitByPipeline(obj.getResponseBody().toString());
        assertEquals("t4", data[0]);
        assertEquals("t5", data[1]);
        assertEquals(map.get("t6"), data[2]);
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 8. DELETE",
            description = "Simple test for DELETE METHOD @RestController.",
            verb = "DELETE",
            url = "t/t1/t2/t3/t4/t5",
            testDataNameReferences = "map")
    public void testDelete2() {
        HttpExchange obj = serverStarter.call();
        String[] data = BaseUtils.splitByPipeline(obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
        assertEquals("t3", data[0]);
        assertEquals(map.get("t6"), data[1]);
        assertEquals(map.get("t7"), data[2]);
    }

    @Test
    @MethodCase(
            name = "TEST 9. CUSTOM VERB",
            description = "Simple test for any custom METHOD. Request void method.",
            verb = "CUSTOM",
            url = "t/t1/t2/t3/t4")
    public void testCustom() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(BaseUtils.EMPTY_STRING, obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 10. CUSTOM VERB",
            description = "Simple test for any custom METHOD @RestController.",
            verb = "CUSTOM",
            url = "t/t1/t2/t3/t4/t5",
            testDataNameReferences = "map")
    public void testCustom2() {
        HttpExchange obj = serverStarter.call();
        String[] data = BaseUtils.splitByPipeline(obj.getResponseBody().toString());
        assertEquals("t3", data[0]);
        assertEquals(map.get("t6"), data[1]);
        assertEquals(map.get("t7"), data[2]);
        assertEquals(200, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 11. OPTIONS",
            description = "Simple test for OPTIONS METHOD. Return splittable String.",
            url = "t/t1/t2/t3/t4/t5",
            testDataNameReferences = "get")
    public void testOptions() throws CannotConvertResponseException {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        String[] data = BaseUtils.splitByPipeline(obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
        assertEquals("t4", data[0]);
        assertEquals("t5", data[1]);
        Assert.assertEquals(UrlParamUtils.splitParamsToMap(get).get("t6"), data[2]);
    }

    @Test
    @MethodCase(
            name = "TEST 12. OPTIONS return void",
            description = "Simple test for OPTIONS METHOD. Request void method.",
            url = "t/t1/t2/t3/t4")
    public void testOptions2() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(BaseUtils.EMPTY_STRING, obj.getResponseBody().toString());
        assertEquals(200, obj.getResponseCode());
    }
}
