package com.example;

import com.example.exception.TestException;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.exception.AbstractPrintTraceException;
import http.server.listener.DefaultHTTPListener;
import http.server.server.MockHTTPServerStarter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@Case(url = "/path/test/test/test")
public class TestControllerThrowExceptionTest {

    private static MockHTTPServerStarter serverStarter;
    private final static String POINT = ".";

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }

    @Test
    @MethodCase(
            name = "TEST 1. GET",
            description = "Test for GET METHOD throws exception processed in method @ExceptionHandler." +
                    " E extend AbstractPrintTraceException.",
            testDataNameReferences = {"get"})
    public void testGET() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        Map<String, Object> data = new Gson().fromJson(obj.getResponseBody().toString(), Map.class);
        assertEquals(3, data.size());
        assertEquals(TestControllerThrowException.METHOD_EXCEPTION_MSG, data.get("message"));
        assertEquals(null, data.get("debugMessage"));
        List<Object> stackTraceList = new Gson().fromJson(data.get("stackTrace").toString(), List.class);
        assertFalse(stackTraceList.isEmpty());
        assertEquals(
                AbstractPrintTraceException.class.getName(),
                new Gson().fromJson(stackTraceList.get(1).toString(), Map.class).get("declaringClass"));
        assertEquals(
                TestException.class.getName(),
                new Gson().fromJson(stackTraceList.get(2).toString(), Map.class).get("declaringClass"));
        assertEquals(
                TestControllerThrowException.class.getName(),
                new Gson().fromJson(stackTraceList.get(3).toString(), Map.class).get("declaringClass"));
        assertEquals(
                "get",
                new Gson().fromJson(stackTraceList.get(3).toString(), Map.class).get("methodName"));
        assertEquals(400, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 2. POST",
            description = "Test for POST METHOD throws exception processed in method @ExceptionHandler." +
                    " E doesn't extend AbstractPrintTraceException and has no param isPrintStackTrace = true.",
            verb = "POST")
    public void testPOST() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(TestControllerThrowException.CUSTOM_EXCEPTION_MSG, obj.getResponseBody().toString());
        assertEquals(444, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 3. PUT",
            description = "Test for PUT METHOD throws exception processed in method @ExceptionHandler." +
                    " E doesn't extend AbstractPrintTraceException and has param isPrintStackTrace.",
            verb = "PUT")
    public void testPUT() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(DefaultHTTPListener.WRONG_COMBINATION_EHANDLER_MESSAGE, obj.getResponseBody().toString());
        assertEquals(410, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 4. DELETE",
            description = "Test for DELETE METHOD throws exception processed in method @ExceptionHandler." +
                    " E doesn't extend AbstractPrintTraceException and has param isPrintStackTrace = false.",
            verb = "DELETE")
    public void testDelete() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(TestControllerThrowException.CUSTOM_EXCEPTION_MSG, obj.getResponseBody().toString());
        assertEquals(416, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 5. OPTIONS",
            description = "Test for OPTIONS VERB throws exception processed in method @ExceptionHandler." +
                    " E doesn't extend AbstractPrintTraceException and has param isPrintStackTrace.",
            verb = "OPTIONS")
    public void testDefault() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals(DefaultHTTPListener.WRONG_COMBINATION_EHANDLER_MESSAGE, obj.getResponseBody().toString());
        assertEquals(410, obj.getResponseCode());
    }

    @Test
    @MethodCase(
            name = "TEST 6. ONERROR (return String).",
            description = "Test for @CustomPoint throws Exception handling by default @OnError at controller.",
            verb = "ONERROR"
    )
    public void testCustomPointOnError() {
        HttpExchange obj = serverStarter.call();
        assertEquals(TestControllerThrowException.ON_ERROR_DEFAULT_MSG, obj.getResponseBody().toString());
        assertEquals(501, obj.getResponseCode());
    }

}
