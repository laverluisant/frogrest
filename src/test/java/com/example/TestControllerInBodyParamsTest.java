package com.example;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.annotation.test.TestData;
import http.server.server.MockHTTPServerStarter;
import http.server.util.BaseUtils;
import http.server.util.enums.TestDataTypeEnum;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Case(url = "path/test2/test3/param")
public class TestControllerInBodyParamsTest {

    private static MockHTTPServerStarter serverStarter;

    @TestData(type = TestDataTypeEnum.Constants.MAP, name = "map")
    public static HashMap<String, String> post = new HashMap<>() {{
        put("map", "map");
        put("post1", (new ArrayList<String>() {{
            add("post2");
            add("post3");
        }}).toString());
    }};

    @TestData(type = TestDataTypeEnum.Constants.MAP, name = "put")
    public static HashMap<String, String> put = new HashMap<>() {{
        put("put", "put");
        put("put1", (new HashMap<String, String>() {{
            put("put2", "put3");
            put("put4", "put5");
            put("put6", "put7");
        }}).toString());
    }};

    @TestData(type = TestDataTypeEnum.Constants.SPLITABLE_STRING, name = "get")
    public static String get = "test1=something&test2=anything";

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }

    @Test
    @MethodCase(
            name = "TEST 1. GET",
            description = "Simple test for GET METHOD. Check inbody params parser.",
            testDataNameReferences = {"get"})
    public void testGET() {
        HttpExchange obj = serverStarter.call();
        Map<String, Object> data = new Gson().fromJson(obj.getResponseBody().toString(), Map.class);
        assertNotNull(data);
        assertEquals(2, data.size());
        assertEquals("something", data.get("test1"));
        assertEquals("anything", data.get("test2"));
    }

    @Test
    @MethodCase(
            name = "TEST 2. POST",
            description = "Simple test for POST METHOD. Check inbody params parser.",
            verb = "POST",
            testDataNameReferences = "map")
    public void testPOST() {
        HttpExchange obj = serverStarter.call();
        Map<String, Object> data = new Gson().fromJson(obj.getResponseBody().toString(), Map.class);
        assertEquals(post.size(), data.size());
        assertEquals(post.get("map"), data.get("map"));
        assertEquals(post.get("post1"), data.get("post1").toString());
    }

    @Test
    @MethodCase(
            name = "TEST 3. PUT",
            description = "Simple test for PUT METHOD. Check inbody params parser.",
            verb = "PUT",
            testDataNameReferences = "put")
    public void testPUT() {
        HttpExchange obj = serverStarter.call();
        Map<String, Object> data = new Gson().fromJson(obj.getResponseBody().toString(), Map.class);
        assertNotNull(data);
        assertEquals(2, data.size());
        assertEquals(put.get("put"), data.get("put"));
        Map<String, String> subData = new Gson().fromJson(data.get("put1").toString(), Map.class);
        assertEquals("put3", subData.get("put2"));
        assertEquals("put5", subData.get("put4"));
        assertEquals("put7", subData.get("put6"));
    }

    @Test
    @MethodCase(
            name = "TEST 4. DELETE",
            description = "Simple test for DELETE METHOD. Check inbody params parser.",
            verb = "DELETE")
    public void testDelete() {
        HttpExchange obj = serverStarter.call();
        assertEquals(200, obj.getResponseCode());
        assertEquals("{}", obj.getResponseBody().toString());
    }

    @Test
    @MethodCase(
            name = "TEST 6. ONERROR (return String).",
            description = "Test for @CustomPoint throws Exception handling by default @OnError at controller.",
            verb = "ONERROR"
    )
    public void testCustomPointOnError() {
        HttpExchange obj = serverStarter.call();
        assertEquals(BaseUtils.DEFAULT_ERROR_MESSAGE, obj.getResponseBody().toString());
        assertEquals(501, obj.getResponseCode());
    }

}