package com.example;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.annotation.test.TestData;
import http.server.exception.CannotConvertResponseException;
import http.server.server.MockHTTPServerStarter;
import http.server.util.BaseUtils;
import http.server.util.UrlParamUtils;
import http.server.util.enums.TestDataTypeEnum;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Case
public class TestControllerVersionTest2 {
    private static MockHTTPServerStarter serverStarter;

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }

    @Test
    @MethodCase(
            name = "TEST 1. Version logic - GET",
            description = "Test version logic for rest controller. Should return value for v2",
            url = "versions",
            versionId = 2)
    public void testGET() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals("\"version 2\"", obj.getResponseBody().toString());
    }

    @Test
    @MethodCase(
            name = "TEST 1. Version logic - POST",
            description = "Test version logic for rest controller. Should return value for v2",
            verb = "POST",
            url = "versions",
            versionId = 2)
    public void testPOST() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals("\"version 2\"", obj.getResponseBody().toString());
    }

    @Test
    @MethodCase(
            name = "TEST 1. Version logic - PUT",
            description = "Test version logic for rest controller. Should return value for v2",
            verb = "PUT",
            url = "versions",
            versionId = 2)
    public void testPUT() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals("\"version 2\"", obj.getResponseBody().toString());
    }

    @Test
    @MethodCase(
            name = "TEST 1. Version logic - DELETE",
            description = "Test version logic for rest controller. Should return value for v2",
            verb = "DELETE",
            url = "versions",
            versionId = 2)
    public void testDelete() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals("\"version 2\"", obj.getResponseBody().toString());
    }

    @Test
    @MethodCase(
            name = "TEST 1. Version logic - CUSTOM VERB",
            description = "Test version logic for rest controller. Should return value for v2",
            verb = "CUSTOM",
            url = "versions",
            testDataNameReferences = "map",
            versionId = 2)
    public void testCustom2() {
        HttpExchange obj = serverStarter.call();
        assertNotNull(obj);
        assertEquals("\"version 2\"", obj.getResponseBody().toString());
    }
}
