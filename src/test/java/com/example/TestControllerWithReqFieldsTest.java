package com.example;

import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.server.MockHTTPServerStarter;
import http.server.util.BaseUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@Case(url = "/test/test2")
public class TestControllerWithReqFieldsTest {
    private static MockHTTPServerStarter serverStarter;

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }

    private void checkDefaultErrorMessage(HttpExchange obj) {
        assertNotNull(obj);
        assertEquals(BaseUtils.getDefaultErrorMessage(), obj.getResponseBody().toString());
    }

    @Test
    @MethodCase(
            name = "TEST 1. GET",
            description = "Simple test for GET METHOD with required field. Should return default onError message." +
                    " Check required field existing.")
    public void testGET() {
        HttpExchange obj = serverStarter.call();
        checkDefaultErrorMessage(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 2. POST",
            description = "Simple test for POST METHOD with required field. Should return default onError message." +
                    " Check required field existing.",
            verb = "POST")
    public void testPOST() {
        HttpExchange obj = serverStarter.call();
        checkDefaultErrorMessage(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 3. PUT",
            description = "Simple test for PUT METHOD with required field. Should return default onError message." +
                    " Check required field existing.",
            verb = "PUT")
    public void testPUT() {
        HttpExchange obj = serverStarter.call();
        checkDefaultErrorMessage(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 4. DELETE",
            description = "Simple test for PUT METHOD with required field. Should return default onError message." +
                    " Check inbody params parser.",
            verb = "DELETE")
    public void testDelete() {
        HttpExchange obj = serverStarter.call();
        checkDefaultErrorMessage(obj);
    }

    @Test
    @MethodCase(
            name = "TEST 5. DEFAULT VERB",
            description = "Simple test for custom METHOD with required field. Should return default onError message." +
                    " Check inbody params parser.",
            verb = "TEST")
    public void testCustom() {
        HttpExchange obj = serverStarter.call();
        checkDefaultErrorMessage(obj);
    }
}
