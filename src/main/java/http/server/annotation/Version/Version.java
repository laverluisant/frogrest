package http.server.annotation.Version;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks controller as version available to allow adding version identifier as
 * first part of rest endpoint. {@link Version#id()} must be annotated. If no
 * word is annotated at {@link Version#word()} version in endpoint would have
 * 'v'+ {@link Version#id()} value.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Version {
    /**
     * Mandatory field which sets version number.
     */
    int id();

    /**
     * Optional field which would be used together with {@link Version#id()}
     * in endpoint building. If no value were set would be used 'v'.
     */
    String word() default "v";
}
