package http.server.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Describe income parameter for method field.
 * Assign params from request body.
 *
 * Created by laverluisant on 27.10.17.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface Param {
    String name();

    Class type() default Object.class;

    boolean required() default false;
}
