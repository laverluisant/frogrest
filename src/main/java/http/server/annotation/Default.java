package http.server.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by laverluisant on 4.11.17.
 *
 * {@link Default} annotation means OPTIONS access. Should be unique in class terms or
 * throws {@link http.server.exception.runtime.DuplicationDefaultAnnotationRuntimeException}.
 * The OPTIONS method represents a request for information about the communication options available
 * on the request/response chain identified by the Request-URI. This method allows the client to determine
 * the options and/or requirements associated with a resource, or the capabilities of a server, without
 * implying a resource action or initiating a resource retrieval.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Default {
}
