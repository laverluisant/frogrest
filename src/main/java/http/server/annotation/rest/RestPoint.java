package http.server.annotation.rest;

import http.server.util.BaseUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks method in {@link RestController} to identify method as rest endpoint.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RestPoint {
    String endpoint() default BaseUtils.EMPTY_STRING;

    String verb() default BaseUtils.EMPTY_STRING;
}
