package http.server.annotation.crud;

import http.server.util.BaseUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines controller with customization in method's hierarchy.
 * And to method custom verb.
 *
 * Created by laverluisant on 25.10.17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface CustomPoint {
    String[] verbs() default BaseUtils.EMPTY_STRING;

    String verb() default BaseUtils.EMPTY_STRING;
}
