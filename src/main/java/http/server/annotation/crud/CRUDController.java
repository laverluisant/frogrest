package http.server.annotation.crud;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Allows for annotated classes to be autodetected through annotation scanning as crud controller.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CRUDController {
    boolean isCustomizable() default false;

    String endpoint() default "/";
}

