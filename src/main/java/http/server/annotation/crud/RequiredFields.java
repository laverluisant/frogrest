package http.server.annotation.crud;

import http.server.util.BaseUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Allows to define the list of required parameters for each http method.
 *
 * Created by laverluisant on 17.05.17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequiredFields {
    /**
     * @return array of required fields.
     */
    String[] fields() default BaseUtils.EMPTY_STRING;
}
