package http.server.annotation;

import http.server.util.BaseUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by laverluisant on 27.10.17.
 *
 * Describe income parameter for method field.
 * Assign params from request endpoint variables.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface PathVariable {
    String name() default BaseUtils.EMPTY_STRING;
}
