package http.server.annotation;

import http.server.annotation.crud.CRUDController;
import http.server.util.BaseUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Allows to determine methods for exception handling in controller.
 * Give possibility to customize exception handling. On exception class can be used only once with each endpoint.
 * If handling for exception doesn't exist would be taken method annotated {@link OnError}. If it doesn't exist
 * too would be invoke {@link BaseUtils#onError()}.
 * It is obligatorily used in combination with annotation {@link CRUDController}
 * or {@link http.server.annotation.rest.RestController}.
 * <p>
 * Created by laverluisant on 26.05.17.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ExceptionHandler {
    /**
     * Defines array of Exception type to be processed in annotated method.
     *
     * @return {@link Class<? extends Exception>} instance of which would be processed.
     */
    Class<? extends Exception>[] exceptions() default Exception.class;

    int status() default 501;

    /**
     * Define returned json representation of {@link http.server.exception.AbstractPrintTraceException} if true
     * or Object/void from appropriate method if false.
     * If method has {@link ExceptionHandler#isPrintStackTrace() == true} but processing exception isn't child
     * of {@link http.server.exception.AbstractPrintTraceException} send back
     * {@link http.server.listener.DefaultHTTPListener#WRONG_COMBINATION_EHANDLER_MESSAGE} message.
     *
     * @return true/false.
     */
    boolean isPrintStackTrace() default false;
}
