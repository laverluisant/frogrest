package http.server.annotation.test;

import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines test data field. Used in combination with {@link Case}, {@link MethodCase}
 * and {@link org.junit.Test} annotations.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TestData {
    /**
     * Defines data type for parser.
     *
     * @return {@link http.server.util.enums.TestDataTypeEnum.Constants}
     */
    String type();

    /**
     * Defines data type name which would be used in {@link MethodCase#testDataNameReferences()}
     * to bind test data with test method which must have {@link MethodCase} and {@link org.junit.Test}
     * annotations.
     *
     * @return test data name.
     */
    String name();
}
