package http.server.annotation.test;

import http.server.annotation.Version.Version;
import http.server.util.BaseUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines methods as case. Use in combination with {@link Case} and {@link org.junit.Test}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MethodCase {
    /**
     * @return case name.
     */
    String name() default BaseUtils.EMPTY_STRING;

    /**
     * @return case description.
     */
    String description() default BaseUtils.EMPTY_STRING;

    /**
     * @return verb for request. Default value is {@link http.server.handler.impl.CrudEndpointHandlerEnum}
     */
    String verb() default "GET";

    /**
     * Binds test data fields to case.
     *
     * @return array of {@link TestData#name()}
     */
    // todo now possible to use only one value - realize multi params usage
    String[] testDataNameReferences() default "";

    /**
     * @return key-url for current method.
     */
    String url() default BaseUtils.EMPTY_STRING;

    /**
     * Set header reference by name to request header TestData.
     *
     * @return reference to {@link TestData#name()} annotated
     * only with {@link http.server.util.enums.TestDataTypeEnum.Constants#HEADER}.
     */
    String testDataRequestHeader() default "";

    /**
     * Optional field which sets version number which was set in controller via
     * {@link Version#id()} to achieve endpoint with version identifiers.
     */
    int versionId() default -1;

    /**
     * Optional field which sets version word which was set together with {@link Version#id()} in controller via
     * {@link Version#word()} to achieve endpoint with version identifiers. Default value is 'v'.
     */
    String versionWord() default  "v";
}
