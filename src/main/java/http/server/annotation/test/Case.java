package http.server.annotation.test;

import http.server.annotation.rest.RestController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines class used tor test controller-class annotated as {@link http.server.annotation.crud.CRUDController}
 * and {@link http.server.annotation.rest.RestController} with key to bind as {@link Case#url()}.
 * Used in combination with {@link MethodCase} and {@link TestData}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Case {
    /**
     * @return key to bind with {@link http.server.annotation.crud.CRUDController#endpoint()} or
     * {@link RestController#endpoint()} depends on type of controller annotation
     */
    String url() default "/";
}
