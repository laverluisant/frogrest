package http.server.handler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import http.server.annotation.Param;
import http.server.bo.ControllerDescription;
import http.server.bo.EndpointBo;
import http.server.bo.FieldDescriptionBo;
import http.server.exception.AbsentRequiredParamsException;
import http.server.exception.CannotConvertResponseException;
import http.server.exception.CantRecognizeMethodException;
import http.server.handler.impl.RestEndpointHandlerEnum;
import http.server.listener.DefaultHTTPListener;
import http.server.util.MethodParamUtils;
import http.server.util.UrlParamUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * RestPoint handler.
 */
public interface Handler {
    String getCurrentVerb();

    void setCurrentVerb(String currentVerb);

    void setBeforeMethod(Method beforeMethod);

    void setAfterMethod(Method afterMethod);

    Method getBeforeMethod();

    Method getAfterMethod();


    /**
     * Return {@link Set} of {@link EndpointBo}.
     *
     * @return {@link Set} of {@link EndpointBo}
     */
    Set<EndpointBo> getEndpoints();

    /**
     * Set {@link Set} of {@link EndpointBo} form {@link DefaultHTTPListener#getControllers()} keyset.
     *
     * @param endpoints {@link Set} of {@link EndpointBo}.
     */
    void setEndpoints(Set<EndpointBo> endpoints);

    /**
     * @param exchange              instance of {@link HttpExchange} contains request data.
     * @param endpointBo            instance of {@link EndpointBo} contains url info.
     * @param controllerDescription instance of {@link ControllerDescription}
     *                              contains bind controller info.
     * @param currentObj            aggregate instance of controller for hole request handling
     * @return response body representation.
     * @throws CantRecognizeMethodException  if {@link Handler} handler can't
     *                                       recognize custom verb.
     * @throws AbsentRequiredParamsException if request params don't contain all
     *                                       required params defines in {@link http.server.annotation.crud.RequiredFields}
     *                                       or marked {@link Param#required() ==  true}.
     *                                       Or can contain some other sneaky exceptions.
     */
    byte[] handle(HttpExchange exchange, EndpointBo endpointBo, ControllerDescription controllerDescription, Object currentObj)
            throws Exception;

    default byte[] methodInvoke(Method method,
                                Class clazz,
                                Map<String, FieldDescriptionBo> methodParameters,
                                Map<String, Object> pathVariables,
                                Map<String, Object> inBodyParamsMap,
                                Headers headers,
                                Object controllerInstance) throws Exception {
        if (getBeforeMethod() != null) {
            getBeforeMethod().invoke(controllerInstance);
        }
        Object obj;
        if (method.getReturnType().equals(Void.TYPE)) {
            if (method.getParameterCount() == 0) {

                method.invoke(controllerInstance);

            } else {
                method.invoke(
                        controllerInstance,
                        MethodParamUtils.createMethodParams(methodParameters, pathVariables, inBodyParamsMap, headers));
            }
            if (getAfterMethod() != null) {
                getAfterMethod().invoke(controllerInstance);
            }
            return new byte[0];
        } else {

            if (method.getParameterCount() == 0) {

                obj = method.invoke(controllerInstance);
            } else {
                obj = method.invoke(
                        controllerInstance,
                        MethodParamUtils.createMethodParams(methodParameters, pathVariables, inBodyParamsMap, headers));

            }
            if (getAfterMethod() != null) {
                getAfterMethod().invoke(controllerInstance);
            }
            return new Gson().toJson(obj).getBytes();
        }
    }

    /**
     * Parser for response query to get {@link Map} of inbody params.
     *
     * @param exchange instance of {@link HttpExchange} from request.
     * @return {@link Map}{@literal <}{@link String}, {@link Object}{@literal >} of inbody params
     * where key is a name of params and value is income object.
     * @throws CannotConvertResponseException if was got bad data.
     */
    default Map parseInBodyParams(HttpExchange exchange) throws CannotConvertResponseException {
        Map inBodyParamsMap;
        if (getCurrentVerb().equals(RestEndpointHandlerEnum.Constants.GET)) {
            inBodyParamsMap = UrlParamUtils.splitParamsToMap(exchange.getRequestURI().getQuery());
        } else {
            String inBody = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                    .lines().collect(Collectors.joining("\n"));
            Type type = new TypeToken<Map<String, Object>>() {
            }.getType();
            inBodyParamsMap = new Gson().fromJson(inBody, type);
        }
        return inBodyParamsMap != null ? inBodyParamsMap : new HashMap();
    }
}
