package http.server.handler.impl;

import com.sun.net.httpserver.HttpExchange;
import http.server.bo.ControllerDescription;
import http.server.bo.EndpointBo;
import http.server.bo.FieldDescriptionBo;
import http.server.exception.CantRecognizeMethodException;
import http.server.handler.Handler;
import http.server.util.MethodParamUtils;
import http.server.util.UrlParamUtils;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Base http rest handler for crud controller.
 * Applied to class annotated {@link http.server.annotation.crud.CRUDController}.
 */
public enum CrudEndpointHandlerEnum implements Handler {
    /**
     * Default handler for crud controller
     */
    DEFAULT {
        @Override
        public byte[] handle(HttpExchange exchange,
                             EndpointBo endpointBo,
                             ControllerDescription controllerDescription,
                             Object controllerInstance) throws Exception {
            Map inBodyParamsMap = parseInBodyParams(exchange);
            Map pathVariables = UrlParamUtils.getPathVariables(endpointBo, new HashSet() {{
                add(controllerDescription.getEndpointBo());
            }});
            setAfterMethod(controllerDescription.getMethodDescription().get(getCurrentVerb()).getAfterMethod());
            setBeforeMethod(controllerDescription.getMethodDescription().get(getCurrentVerb()).getBeforeMethod());
            UrlParamUtils.checkAllRequiredParamExist(inBodyParamsMap,
                    controllerDescription.getMethodDescription().get(getCurrentVerb()).getRequiredParams());
            if (controllerDescription.getMethodDescription().get(getCurrentVerb()).getResponseHeaders() != null)
                exchange.getResponseHeaders().putAll(
                        controllerDescription.getMethodDescription().get(getCurrentVerb()).getResponseHeaders());
            Set<String> verbs = controllerDescription.getMethodDescription().keySet();
            if (verbs.contains(getCurrentVerb())) {
                Method method = controllerDescription.getMethodDescription().get(getCurrentVerb()).getCertainMethod();
                Map<String, FieldDescriptionBo> methodParameters =
                        MethodParamUtils.getMethodParameters(method.getParameters());
                return methodInvoke(method,
                        controllerDescription.getMethodDescription().get(getCurrentVerb()).getCertainClazz(),
                        methodParameters,
                        pathVariables,
                        inBodyParamsMap,
                        exchange.getRequestHeaders(),
                        controllerInstance);
            }
            throw new CantRecognizeMethodException(String.format("Server doesn't recognize %s method", getCurrentVerb()));
        }
    };

    private String currentVerb;
    private Set<EndpointBo> endpoints;
    private Method beforeMethod;
    private Method afterMethod;

    @Override
    public void setBeforeMethod(Method beforeMethod) {
        this.beforeMethod = beforeMethod;
    }

    @Override
    public void setAfterMethod(Method afterMethod) {
        this.afterMethod = afterMethod;
    }

    @Override
    public Method getBeforeMethod() {
        return beforeMethod;
    }

    @Override
    public Method getAfterMethod() {
        return afterMethod;
    }

    @Override
    public String getCurrentVerb() {
        return currentVerb;
    }

    @Override
    public void setCurrentVerb(String currentVerb) {
        this.currentVerb = currentVerb.toUpperCase();
    }

    @Override
    public Set<EndpointBo> getEndpoints() {
        return endpoints;
    }

    @Override
    public void setEndpoints(Set<EndpointBo> endpoints) {
        this.endpoints = endpoints;
    }
}
