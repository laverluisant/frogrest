package http.server.server;

import java.io.IOException;

/**
 * Created by laverluisant on 10.06.17.
 */
public interface ServerStarter {

    default void afterStart(){
        System.out.println();
        System.out.println("                 |###################|");
        System.out.println("                 |###################|");
        System.out.println("                 |###################|");
        System.out.println("                 |###################|");
        System.out.println("     ((-----------------------------------------");
        System.out.println("     | \\         /  /@@ \\      /@@ \\  \\");
        System.out.println("      \\ \\,      /  (     )    (     )  \\            _____");
        System.out.println("       \\ \\      |   \\___/      \\___/   |           /  __ \\");
        System.out.println("        \\ \"\"*-__/                      \\           | |  | |");
        System.out.println("         \"\"*-_                         \"-_         | |  \"\"\"");
        System.out.println("              \\    -.  _________   .-   __\"-.__.-((  ))");
        System.out.println("               \\,    \\^    U    ^/     /  \"-___--((  ))");
        System.out.println("                 \\,   \\         /    /'            | |");
        System.out.println("                  |    \\       /   /'              | |");
        System.out.println("                  |     \"-----\"    \\               | |");
        System.out.println("                 /                  \"*-._          | |");
        System.out.println("                /   /\\          /*-._    \\         | |");
        System.out.println("               /   /  \"\\______/\"     /   /         | |");
        System.out.println("              /   /                 /   /          | |");
        System.out.println("             /. ./                  |. .|          \"\"\"");
        System.out.println("            /  | |                  / | \\");
        System.out.println("           /   |  \\                /  |  \\");
        System.out.println("          /.-./.-.|               /.-.|.-.\\");
        System.out.println();
    }
    /**
     * Start server.
     */
    void start();

    /**
     * Stop server.
     */
    void stop();
}
