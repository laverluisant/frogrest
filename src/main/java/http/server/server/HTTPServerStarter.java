package http.server.server;

import com.sun.net.httpserver.HttpServer;
import http.server.exception.runtime.EmptyControllersMapRuntimeException;
import http.server.listener.DefaultHTTPListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Allows to create simple http GET server. Default port to run is 8765.
 * <p>
 * Created by laverluisant on 17.05.17.
 */
public class HTTPServerStarter implements ServerStarter {
    private final static int PORT = 8765;
    private final static int THREADS = 10;
    private ExecutorService httpThreadPool;
    private HttpServer server;
    private List<String> packages;
    private int port;
    private int threads;

    public List<String> getPackages() {
        return packages;
    }

    public int getPort() {
        return port;
    }

    public int getThreads() {
        return threads;
    }

    /**
     * @param packages array of packages for scanning
     */
    public HTTPServerStarter(List<String> packages) {
        this(packages, PORT, THREADS);
    }

    /**
     * @param packages array of packages for scanning
     * @param port     port for running
     * @param threads  quantity of running threads
     */
    public HTTPServerStarter(List<String> packages, int port, int threads) {
        this.packages = packages;
        this.port = port;
        this.threads = threads;
    }

    /**
     * Start http server.
     *
     */
    @Override
    public void start() {
        try {
            this.server = HttpServer.create();
            server.bind(new InetSocketAddress(port), 0);
        } catch (IOException e) {}
        DefaultHTTPListener listener;
            listener = new DefaultHTTPListener(this.packages);
        if (listener.getControllers().size() == 0) {
            throw new EmptyControllersMapRuntimeException();
        }
        this.httpThreadPool = Executors.newFixedThreadPool(threads);
        this.server.setExecutor(this.httpThreadPool);
        server.createContext("/", listener);
        server.setExecutor(null);
        server.start();
        this.afterStart();
        System.out.println(String.format("Server started at port - %s.", PORT));
    }

    /**
     * Stop http server.
     */
    @Override
    public void stop() {
        this.server.stop(1);
        this.httpThreadPool.shutdownNow();
    }
}
