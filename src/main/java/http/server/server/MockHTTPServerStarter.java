package http.server.server;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.annotation.test.TestData;
import http.server.exception.runtime.TestDataParserRuntimeExcetion;
import http.server.exception.runtime.WrongAnnotationCombinationRuntimeException;
import http.server.exception.runtime.WrongTestDataRuntimeException;
import http.server.listener.DefaultHTTPListener;
import http.server.util.BaseUtils;
import http.server.util.enums.TestDataTypeEnum;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Mock implementation of {@link ServerStarter} used in test. Should be created and initialized
 * from {@link org.junit.BeforeClass} annotated method.
 * <p>
 * Important!
 * Controller and ControllerTest classes should allocate in same package or starter won't be able to
 * find Controller class.
 * ControllerTest.class should be annotated {@link Case} and {@link Test} or
 * would be thrown runtime exception {@link WrongAnnotationCombinationRuntimeException}.
 */
public class MockHTTPServerStarter implements ServerStarter {
    private Class clazz;
    private DefaultHTTPListener defaultHTTPListener;
    private MockHttpExchange httpExchange;
    private final String URI;
    private String CURRENT_URI;
    private Map<String, Method> existingMethods;
    private Map<String, String> testData;
    private Map<String, Headers> requestHeaders;

    public MockHTTPServerStarter() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        try {
            this.clazz = Class.forName(trace[2].getClassName());
        } catch (ClassNotFoundException e) {
        }
        if (!(clazz.isAnnotationPresent(Case.class))) {
            throw new WrongAnnotationCombinationRuntimeException(
                    String.format("In %s.class missed annotation %s ",
                            clazz.getName(), Case.class.getName())
            );
        }
        this.defaultHTTPListener = new DefaultHTTPListener(
                Arrays.asList(clazz.getPackageName()));
        this.URI = ((Case) clazz.getAnnotation(Case.class)).url();
        this.CURRENT_URI = URI;
    }


    /**
     * Start mock server. Should be invoked from {@link org.junit.BeforeClass} annotated method.
     * First, in side creates and stores test data to be used as request params. Uses for that building
     * {@link TestData} annotated fields.
     * Second, creates and stores array of future invoking method with annotation {@link MethodCase}.
     */
    @Override
    public void start() {
        existingMethods = Arrays.stream(clazz.getMethods())
                .filter(method -> method.isAnnotationPresent(MethodCase.class))
                .collect(Collectors.toMap(m -> m.getName(), m -> m));
        testData = Arrays.stream(clazz.getFields())
                .filter(field -> field.isAnnotationPresent(TestData.class))
                .filter(f ->
                        TestDataTypeEnum.Constants.getConstantsForNameReferences().contains(f.getAnnotation(TestData.class).type()))
                .collect(Collectors.toMap(f -> f.getAnnotation(TestData.class).name(), f -> {
                    try {
                        TestData tdAnnotation = f.getAnnotation(TestData.class);
                        TestDataTypeEnum tdEnum = (TestDataTypeEnum) BaseUtils.getValueByStringOrDefault(
                                tdAnnotation.type(), null, TestDataTypeEnum.class);
                        if (f.getType().isInterface()) {
                            throw new WrongTestDataRuntimeException(
                                    String.format("Wrong type in @TestData name = '%s' at %s.class. TestData can't be declared as an Interface."
                                            , tdAnnotation.name(), clazz.getName()));
                        }
                        return (String) tdEnum.checkAndModifyValue(f);
                    } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                        throw new TestDataParserRuntimeExcetion();
                    }
                }));
        requestHeaders = Arrays.stream(clazz.getFields())
                .filter(field -> field.isAnnotationPresent(TestData.class))
                .filter(f ->
                        TestDataTypeEnum.Constants.getConstantForHeadersReferences().contains(f.getAnnotation(TestData.class).type()))
                .collect(Collectors.toMap(ff -> ff.getAnnotation(TestData.class).name(), ff -> {
                    try {
                        return (Headers) TestDataTypeEnum.HEADER.checkAndModifyValue(ff);
                    } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    return new Headers();
                }));
    }

    @Override
    public void stop() {
    }

    /**
     * Used inside case method annotated {@link MethodCase} and {@link org.junit.Test}
     * to send 'mock' rest request.
     *
     * @return server response object.
     */
    public HttpExchange call() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        Method method = existingMethods.get(trace[2].getMethodName());
        prepareMockData(method);
        return httpExchange;
    }

    private void prepareMockData(Method method) {
        MethodCase methodCase = method.getAnnotation(MethodCase.class);
        String verb = methodCase.verb();
        CURRENT_URI = URI;
        if (!methodCase.url().equals("")) {
            if (methodCase.versionId() != -1)
                CURRENT_URI = methodCase.versionWord() + methodCase.versionId() + "/" + methodCase.url();
            else
                CURRENT_URI = methodCase.url();
        }
        this.httpExchange = new MockHttpExchange(methodCase.name(), methodCase.description(), verb);
        // TODO add group testData params
        if ("GET".equals(verb)) {
            CURRENT_URI += "?" + testData.get(methodCase.testDataNameReferences()[0]);
        }
        httpExchange.setObject(testData.get(methodCase.testDataNameReferences()[0]));
        httpExchange.setRequestHeader(requestHeaders.get(methodCase.testDataRequestHeader()));
        httpExchange.setRequestMethod(verb);
        try {
            defaultHTTPListener.handle(httpExchange);
        } catch (IOException e) {
        }
        CURRENT_URI = URI;
    }

    /**
     * Mock {@link HttpExchange} implementation.
     */
    public class MockHttpExchange extends HttpExchange {
        private OutputStream uos;
        private ByteArrayOutputStream uosOrigin;
        private String requestMethod;
        private String json;
        private Headers requestHeader;
        private Headers rsHeader = new Headers();
        private int code;

        public MockHttpExchange(String name, String desc, String verb) {
            System.out.println(String.format("%s - %s", name, desc));
            System.out.println(String.format("URI - %s, method - %s.", CURRENT_URI, verb));
        }

        @Override
        public Headers getRequestHeaders() {
            return requestHeader;
        }

        public void setRequestHeader(Headers requestHeader) {
            this.requestHeader = requestHeader;
        }

        @Override
        public Headers getResponseHeaders() {
            return rsHeader;
        }

        @Override
        public URI getRequestURI() {
            try {
                return new URI(CURRENT_URI);
            } catch (URISyntaxException e) {
                return null;
            }
        }

        @Override
        public String getRequestMethod() {
            return this.requestMethod;
        }

        public void setRequestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
        }

        @Override
        public HttpContext getHttpContext() {
            return null;
        }

        @Override
        public void close() {

        }

        @Override
        public InputStream getRequestBody() {
            return new ByteArrayInputStream(json.getBytes());
        }

        public void setObject(String json) {
            this.json = json == null ? "" : json;
        }

        @Override
        public OutputStream getResponseBody() {
            if (uos == null) {
                uosOrigin = new ByteArrayOutputStream();
                uos = uosOrigin;
            }
            return uos;
        }

        @Override
        public void sendResponseHeaders(int rCode, long responseLength) {
            this.code = rCode;
        }

        @Override
        public InetSocketAddress getRemoteAddress() {
            return null;
        }

        @Override
        public int getResponseCode() {
            return code;
        }

        @Override
        public InetSocketAddress getLocalAddress() {
            return null;
        }

        @Override
        public String getProtocol() {
            return null;
        }

        @Override
        public Object getAttribute(String name) {
            return null;
        }

        @Override
        public void setAttribute(String name, Object value) {

        }

        @Override
        public void setStreams(InputStream i, OutputStream o) {

        }

        @Override
        public HttpPrincipal getPrincipal() {
            return null;
        }
    }
}
