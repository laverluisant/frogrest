package http.server.util;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import http.server.annotation.InBodyParamsMap;
import http.server.annotation.Param;
import http.server.annotation.PathVariable;
import http.server.annotation.PathVariablesMap;
import http.server.bo.FieldDescriptionBo;
import http.server.exception.AbsentRequiredParamsException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Method parameter utility. Is used to processing method income parameters.
 * <p>
 * Created by laverluisant on 27.10.17.
 */
public class MethodParamUtils {

    /**
     * Return map of method parameters.
     *
     * @param parameters Array of invoking method parameters.
     * @return {@link Map} <String, {@link FieldDescriptionBo}> with method params (<PARAM_NAME, TYPE>).
     * PARAM_NAME - get from one of these annotation added to method params
     * {@link Param} name() or {@link PathVariable} name().
     */
    public static Map<String, FieldDescriptionBo> getMethodParameters(Parameter[] parameters) {
        Map<String, FieldDescriptionBo> methodParameters = new LinkedHashMap<>();
        for (Parameter param : parameters) {
            Annotation[] annotations = param.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof Param) {
                    methodParameters.put(
                            param.getAnnotation(Param.class).name(),
                            new FieldDescriptionBo(param.getType(), param.getAnnotation(Param.class).required()));
                } else if (annotation instanceof PathVariable) {
                    methodParameters.put(
                            param.getAnnotation(PathVariable.class).name(),
                            new FieldDescriptionBo(param.getType(), false));
                } else if (annotation instanceof InBodyParamsMap) {
                    methodParameters.put(
                            InBodyParamsMap.class.getSimpleName(),
                            new FieldDescriptionBo(param.getType(), false)
                    );
                } else if (annotation instanceof PathVariablesMap) {
                    methodParameters.put(
                            PathVariablesMap.class.getSimpleName(),
                            new FieldDescriptionBo(param.getType(), false)
                    );
                }
            }
            if (param.getType() == Headers.class) {
                methodParameters.put(param.getName(), new FieldDescriptionBo(param.getType(), false));
            }
        }
        return methodParameters;
    }

    /**
     * Create method parameters array.
     *
     * @param methodParameters {@link Map}<String, {@link FieldDescriptionBo}> with necessary params (<NAME, FIELD_DESC>) to invoke method.
     * @param pathVariables    {@link Map<String, Object>} with all parsed path variables(<PARAM_NAME, OBJECT>) from request.
     * @param inBodyParam      {@link Map<String, Object>} with all parsed body params(<PARAM_NAME, OBJECT>) from request.
     * @return array of parameters to invoke method.
     * @throws AbsentRequiredParamsException if param with annotation {@link Param} which marked as required is null.
     */
    public static Object[] createMethodParams(
            Map<String, FieldDescriptionBo> methodParameters,
            Map<String, Object> pathVariables,
            Map<String, Object> inBodyParam,
            Headers headers) throws AbsentRequiredParamsException {
        Object[] params = new Object[methodParameters.size()];
        int i = 0;
        for (String key : methodParameters.keySet()) {
            if (methodParameters.get(key).getClazz() == Headers.class) {
                params[i] = headers;
            } else if (pathVariables != null && pathVariables.containsKey(key)) {
                params[i] = convert(pathVariables.get(key), methodParameters.get(key));
            } else if (inBodyParam != null && inBodyParam.containsKey(key)) {
                params[i] = convert(inBodyParam.get(key), methodParameters.get(key));
            } else if (key.equals(InBodyParamsMap.class.getSimpleName())) {
                params[i] = inBodyParam;
            } else if (key.equals(PathVariablesMap.class.getSimpleName())) {
                params[i] = pathVariables;
            } else {
                params[i] = convert(null, methodParameters.get(key));
            }
            i++;
        }
        // todo Add processing wrong params collections
        return params;
    }

    /**
     * Convert object from request to necessary type for method invokes.
     *
     * @param obj                object from request.
     * @param fieldDescriptionBo {@link FieldDescriptionBo} describe method variable.
     * @return object which is instance of type specified in controller method.
     * @throws AbsentRequiredParamsException if param with annotation {@link Param} which marked as required is null.
     */
    public static Object convert(Object obj, FieldDescriptionBo fieldDescriptionBo) throws AbsentRequiredParamsException {
        if (obj == null && fieldDescriptionBo.isRequered())
            throw new AbsentRequiredParamsException();
        if (obj == null)
            return null;
        Class clazz = fieldDescriptionBo.getClazz();
        return new Gson().fromJson(obj.toString(), clazz);
    }
}
