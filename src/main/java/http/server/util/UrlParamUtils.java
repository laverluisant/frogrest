package http.server.util;

import http.server.bo.EndpointBo;
import http.server.exception.AbsentRequiredParamsException;
import http.server.exception.CannotConvertResponseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Url params utility.
 * <p>
 * Created by laverluisant on 02.06.17.
 */
public class UrlParamUtils {


    /**
     * Checks that all required parameters exist.
     *
     * @param inParams  {@link Map} of request parameters
     * @param reqParams {@link List} of required parameters
     * @return true if all exist
     */
    public static boolean checkAllRequiredParamExist(Map inParams, List reqParams) throws Exception {
        for (int i = 0; i < reqParams.size(); i++) {
            if (!inParams.containsKey(reqParams.get(i))) {
                throw new AbsentRequiredParamsException();
            }
        }
        return true;
    }

    /**
     * Splits parameters in map.
     *
     * @param params row with parameters
     * @return {@link Map} of response parameters
     * @throws CannotConvertResponseException if it's impossible to spit parameters
     */
    public static Map<String, String> splitParamsToMap(String params) throws CannotConvertResponseException {
        Map<String, String> paramsMap = new HashMap<>();
        if (params != null && params.contains("=")) {
            String[] parts = params.split("&");
            for (String part : parts) {
                String[] subParts = part.split("=");
                if (subParts.length == 2) {
                    paramsMap.put(subParts[0], subParts[1]);
                } else {
                    throw new CannotConvertResponseException("Bad is " + parts);
                }
            }
        }
        return paramsMap;
    }

    /**
     * Parses url variable to get path variables.
     *
     * @param endpointBo     {@link EndpointBo} created from requested data.
     * @param controllerKeys {@link Set} of {@link EndpointBo} to parse path variables.
     * @return {@link Map} which contains param name as a key and param value as a value.
     */
    public static Map getPathVariables(EndpointBo endpointBo, Set<EndpointBo> controllerKeys) {
        return EndpointUtils.parsePathVariablesToMap(
                endpointBo,
                controllerKeys
                        .stream()
                        .filter(endpointBo::equals)
                        .findFirst()
                        .orElse(null));
    }
}