package http.server.util;

import http.server.exception.ComingSoonException;

import java.util.*;

/**
 * Base utility.
 * <p>
 * Created by laverluisant on 15.10.17.
 */
public class BaseUtils {

    public static final String EMPTY_STRING = "";
    public static final String AMPERSAND = "&";
    public static final String DEFAULT_ERROR_MESSAGE = "Something goes wrong way!";

    /**
     * Check is {@link Boolean} variable true. Recommend to use with converting to primitive boolean.
     *
     * @param value {@link Boolean} candidate value to be true.
     * @return true if candidate is true.
     */
    public static Boolean isTrue(Boolean value) {
        return value != null && value;
    }

    /**
     * Return Enum value form String of default.
     *
     * @param value        {@link String} value gotta convert to {@link Enum}.
     * @param defaultValue returned value is value doesn't exist.
     * @param clazz        type of Enum.
     * @return Enum value which is equal to value of default.
     */
    public static Enum getValueByStringOrDefault(String value, Enum defaultValue, Class clazz) {
        return Arrays.stream(clazz.getEnumConstants())
                .map(obj -> (Enum) obj)
                .filter(v -> value.equalsIgnoreCase(v.name()))
                .findFirst()
                .orElse(defaultValue);
    }

    /**
     * Check if enum contains value which  is equal to string value.
     *
     * @param enumValue type of Enum.
     * @param value     candidate to be existing.
     * @return true if value exists in current enum.
     */
    public static boolean isEnumContains(Enum enumValue, String value) {
        return Arrays.stream(enumValue.getClass().getEnumConstants())
                .map(obj -> (Enum) obj)
                .anyMatch(v -> value.equalsIgnoreCase(v.name()));
    }

    /**
     * Check if income array contains only unique value.
     *
     * @param objs candidate array.
     * @return true if candidate array contains only unique values.
     */
    public static boolean isUniqueValueArray(Object[] objs) {
        Set set = new HashSet();
        Collections.addAll(set, objs);
        return set.size() == objs.length;
    }

    /**
     * Check for uniqueness {@link List}.
     *
     * @param list candidate to be unique.
     * @return true if list contains only unique value.
     */
    public static boolean isUniqueValueList(List list) {
        return (new HashSet(list)).size() == list.size();
    }

    /**
     * Auxiliary method for slitting {@link String} value by '&'.
     *
     * @param value for splitting.
     * @return array of {@link String} splitted by '&'.
     */
    public static String[] splitByPipeline(String value) {
        return value.replace("\"", EMPTY_STRING).replace("\\u0026", AMPERSAND).split(AMPERSAND);
    }

    /**
     * Default behavior for exceptions. Would be called on {@link ComingSoonException},
     * {@link http.server.exception.CannotConvertResponseException}, {@link IllegalAccessException},
     * {@link java.lang.reflect.InvocationTargetException} and any other which are not annotated with
     * {@link http.server.annotation.ExceptionHandler}.
     *
     * @return Object or subtype which describes exception entity.
     */
    public static Object onError() {
        return DEFAULT_ERROR_MESSAGE;
    }

    /**
     * Return default error message.
     *
     * @return {@link String} error message.
     */
    public static String getDefaultErrorMessage() {
        return DEFAULT_ERROR_MESSAGE;
    }
}
