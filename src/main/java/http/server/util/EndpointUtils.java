package http.server.util;

import http.server.annotation.Param;
import http.server.annotation.PathVariable;
import http.server.annotation.Version.Version;
import http.server.annotation.crud.CustomPoint;
import http.server.annotation.rest.RestPoint;
import http.server.bo.EndpointBo;
import http.server.exception.runtime.DuplicateAnnotedNamesInMethodRuntimeExcetion;
import http.server.exception.runtime.DuplicateVerbRuntimeException;
import http.server.exception.runtime.InvalidEndpointRuntimeException;
import http.server.exception.runtime.WrongAnnotationCombinationRuntimeException;
import http.server.handler.impl.RestEndpointHandlerEnum;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Endpoint utility for creating {@link EndpointBo}
 * <p>
 * Created by laverluisant on 01.06.17.
 */
public class EndpointUtils {
    /**
     * Create {@link EndpointBo} object.
     *
     * @param path  url like String
     * @param clazz class of controller
     * @return unique {@link EndpointBo} object
     */
    public static EndpointBo createEndpoint(String path, Class clazz) {
        if (path == null || path.isEmpty()) {
            throw new InvalidEndpointRuntimeException("Invalid endpoint " + path + " in class " + clazz.getName());
        }
        Version version = (Version) clazz.getAnnotation(Version.class);
        if (version != null)
            path = version.word() + version.id() + "/" + path;
        String[] filteredParts = Arrays
                .stream(path.split("/"))
                .filter(e -> !e.isEmpty())
                .toArray(String[]::new);
        Set<Integer> paths = new HashSet<>();
        for (int i = 0; i < filteredParts.length; i++) {
            if (filteredParts[i].matches("^(\\{.*\\S\\}$)")) {
                paths.add(i);
            }
        }
        return new EndpointBo(
                (filteredParts.length != 0 ?
                        Arrays.stream(filteredParts)
                                .map(p -> p.replace("{", BaseUtils.EMPTY_STRING).replace("}", BaseUtils.EMPTY_STRING)).collect(Collectors.toList())
                        : new ArrayList<>() {{
                    add(BaseUtils.EMPTY_STRING);
                }}),
                paths);
    }

    /**
     * Split string path to {@link EndpointBo}
     *
     * @param path request url in {@link String} format.
     * @return {@link EndpointBo} object with empty in {@link EndpointBo#getPathKeys()}
     */
    public static EndpointBo splitPath(String path) {
        String[] filteredParts = Arrays
                .stream(path.split("/"))
                .filter(e -> !e.isEmpty())
                .toArray(String[]::new);
        return new EndpointBo(
                (filteredParts.length != 0 ?
                        Arrays.stream(filteredParts)
                                .map(p -> p.replace("{", BaseUtils.EMPTY_STRING).replace("}", BaseUtils.EMPTY_STRING)).collect(Collectors.toList())
                        : new ArrayList<>() {{
                    add(BaseUtils.EMPTY_STRING);
                }}),
                Collections.emptySet());
    }

    /**
     * Parse all path variable and collect its in {@link Map}.
     *
     * @param inUrl      object from request endpoint.
     * @param endpointBo object form controller annotation.
     * @return {@link Map} of parameters.
     */
    public static Map parsePathVariablesToMap(EndpointBo inUrl, EndpointBo endpointBo) {
        Map variables = new HashMap();
        if (inUrl != null && endpointBo != null) {
            for (Object key : endpointBo.getPathKeys()) {
                variables.put(endpointBo.getFullKeys().get((int) key), inUrl.getFullKeys().get((int) key));
            }
        }
        return variables;
    }

    /**
     * @param newEndpoint    new endpoint which would be checked.
     * @param existEndpoints set of already existing {@link EndpointBo} objects.
     * @return true if new endpoint unique.
     */
    public static boolean isUniqueEndpoint(EndpointBo newEndpoint, Set<EndpointBo> existEndpoints) {
        if (existEndpoints.isEmpty()) return true;
        if (existEndpoints.contains(newEndpoint)) return false;
        int size = newEndpoint.getFullKeys().size();
        List<EndpointBo> filteredEndpoints = existEndpoints
                .stream()
                .filter(point -> point.getFullKeys().size() == size)
                .filter(defaultPoint -> !(defaultPoint.getFullKeys().size() == 1 && BaseUtils.EMPTY_STRING.equals(defaultPoint.getFullKeys().get(0))))
                .collect(Collectors.toList());
        if (filteredEndpoints.isEmpty()) return true;
        for (EndpointBo endpoint : filteredEndpoints) {
            for (int i = 0; i < size; i++) {
                if (!endpoint.getPathKeys().contains(i)
                        && !newEndpoint.getPathKeys().contains(i)
                        && !endpoint.equals(newEndpoint)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check compatibility of method params.
     *
     * @param method {@link Method} method whick should be check on wrong combination.
     * @throws WrongAnnotationCombinationRuntimeException runtime exception if present incompatible
     *                                                    combination of param annotations.
     */
    public static void checkMethodParamsCompatibility(Method method) {
        Parameter[] methodParams = method.getParameters();
        for (Parameter param : methodParams) {
            Annotation[] annotations = param.getAnnotations();
            if (isContainsBoth(Param.class, PathVariable.class, annotations)) {
                throw new WrongAnnotationCombinationRuntimeException(
                        String.format("Method %s(...) can't contains both %s %s annotation which couldn't be uses together.",
                                method.getName(), Param.class.getName(), PathVariable.class.getName()));
            }

        }
    }

    private static boolean isContainsBoth(Class a1, Class a2, Annotation[] all) {
        boolean isA1Present = false, isA2Present = false;
        for (Annotation a : all) {
            if (a.annotationType() == a1) {
                isA1Present = true;
            } else if (a.annotationType() == a2) {
                isA2Present = true;
            }
        }
        return isA1Present && isA2Present;
    }

    /**
     * Check uniqueness of method params annotated names.
     *
     * @param method {@link Method} method whick should be check on wrong combination.
     * @throws DuplicateAnnotedNamesInMethodRuntimeExcetion runtime exception if present duplicate
     *                                                      params annotated name.
     */
    public static void checkMethodAnnotatedNameUniqueness(Method method) {
        Parameter[] methodParams = method.getParameters();
        List<String> annotatedNames = new ArrayList<>();
        for (Parameter parameter : methodParams) {
            String annotatedName = null;
            if (parameter.getAnnotation(Param.class) != null) {
                annotatedName = parameter.getAnnotation(Param.class).name().toUpperCase();
            } else if (parameter.getAnnotation(PathVariable.class) != null) {
                annotatedName = parameter.getAnnotation(PathVariable.class).name().toUpperCase();
            }
            if (annotatedNames.contains(annotatedName)) {
                throw new DuplicateAnnotedNamesInMethodRuntimeExcetion(
                        String.format("Annotated name %s is duplicate at %s(...)",
                                annotatedName, method.getName()));
            }
            if (annotatedName != null)
                annotatedNames.add(annotatedName);
        }
    }

    /**
     * Validates endpoint candidate.
     *
     * @param endpoint from {@link http.server.annotation.crud.CRUDController#endpoint()} or
     *                 {@link http.server.annotation.rest.RestController}
     * @return true if valid else false.
     */
    public static boolean isValidEndpoint(String endpoint) {
        return endpoint.matches("[a-zA-Z0-9/{}]*");
    }

    /**
     * Validates custom verb candidate. Custom verb should consist of only upper letter and
     * {@link RestEndpointHandlerEnum.Constants#getAllConstants()} shouldn't contain it.
     * Custom verb couldn't be equal to default GET method as GET, POST, PUT, DELETE or
     * default method OPTIONS.
     *
     * @param verb is a candidate form {@link CustomPoint#verb()} to be used as method verb.
     * @return true if valid else false.
     */
    public static boolean isValidEndpointCustomVerb(String verb) {
        return verb.matches("[A-Z]*") &&
                !RestEndpointHandlerEnum.Constants.getAllConstants().contains(verb.toUpperCase());
    }

    /**
     * Validate all annotated verb. Verb should consist of only upper letter.
     *
     * @param verb is a candidate form {@link RestPoint#endpoint()} to be used as a method verb.
     * @return true if valid else false.
     */
    public static boolean isValidEndpointVerb(String verb) {
        return verb.matches("[A-Z]*");
    }

    /**
     * Replaces some wrong simbols from {@link http.server.annotation.crud.CRUDController#endpoint()}
     * or {@link http.server.annotation.rest.RestController}
     *
     * @param endpoint from {@link http.server.annotation.crud.CRUDController#endpoint()} or
     *                 {@link http.server.annotation.rest.RestController}
     * @return reformed endpoint.
     */
    public static String replace(String endpoint) {
        if (endpoint.contains("//")) {
            endpoint = endpoint.replace("//", "/");
            endpoint = replace(endpoint);
        }
        return endpoint;
    }

    /**
     * Seek default method annotation form {@link RestEndpointHandlerEnum.Constants#getAllConstants()}
     *
     * @param annotations
     * @return null - if din't find; one of {@link RestEndpointHandlerEnum.Constants} - if find one
     * @throws DuplicateVerbRuntimeException if find more than one appropriate annotation.
     */
    public static Annotation findCRUDAnnotation(Annotation[] annotations) {
        return Arrays.stream(annotations)
                .filter(a -> RestEndpointHandlerEnum.Constants.getAllConstants().contains(a.annotationType().getSimpleName().toUpperCase()))
                .collect(CollectorUtils.toFirstListElement());
    }
}
