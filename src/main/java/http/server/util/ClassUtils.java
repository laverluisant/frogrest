package http.server.util;

import http.server.annotation.crud.CRUDController;
import http.server.exception.runtime.NoZeroParamsConstructorExistRuntimeException;
import http.server.exception.runtime.UnsupportedEncodingRuntimeException;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class ClassUtils {
    private static final String ENCODING = System.getProperty("file.encoding");
    public final static String NO_ZERO_CONSTRUCTOR_MESSAGE =
            "No zero constructor in %s.class. Every class annotated @%s should have constructor with zero params.";

    public static List<Class> getClasses(ClassLoader cl, List<String> packages) {
        List<Class> classes = new ArrayList<>();
        for (String pack : packages) {
            String dottedPackage = pack.replaceAll("[/]", ".");
            String slashPackage = pack.replaceAll("[.]", "/");
            URL packageUrl = cl.getResource(slashPackage);
            String pathToControllers;
            try {
                pathToControllers = URLDecoder.decode(packageUrl.getPath()
                        .replaceAll("test-", BaseUtils.EMPTY_STRING), ENCODING);
                File directory = new File(pathToControllers);
                File[] files = directory.listFiles();
                for (File file : files) {
                    String name = file.getName().replace(".class", BaseUtils.EMPTY_STRING);
                    try {
                        classes.add(Class.forName(dottedPackage + "." + name));
                    } catch (ClassNotFoundException e) {
                        // add recursion to read in side package
                    }
                }
            } catch (UnsupportedEncodingException e) {
                throw new UnsupportedEncodingRuntimeException(
                        String.format("Error read files from %s", packageUrl.getPath())
                );
            }
        }
        return classes;
    }

    /**
     * Find default constructor.
     *
     * @param clazz - type for seeking zero controller.
     * @return {@link Constructor} with zero param.
     */
    public static Constructor getZeroParamsConstructor(Class clazz) {
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor c : constructors) {
            if (c.getParameterCount() == 0)
                return c;
        }
        throw new NoZeroParamsConstructorExistRuntimeException(
                String.format(NO_ZERO_CONSTRUCTOR_MESSAGE, clazz.getName(), CRUDController.class.getName())
        );
    }

    /**
     * Check if default constructor exists.
     *
     * @param clazz - type for seeking zero controller.
     * @return true if default constructor exists else return false.
     */
    public static boolean isZeroParamsConstructorExist(Class clazz) {
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor c : constructors) {
            if (c.getParameterCount() == 0)
                return true;
        }
        return false;
    }
}
