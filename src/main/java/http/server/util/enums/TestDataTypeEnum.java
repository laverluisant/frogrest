package http.server.util.enums;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import http.server.annotation.test.Case;
import http.server.annotation.test.MethodCase;
import http.server.annotation.test.TestData;
import http.server.exception.CannotConvertResponseException;
import http.server.exception.runtime.WrongTestDataRuntimeException;
import http.server.util.ClassUtils;
import http.server.util.UrlParamUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Test data validator and processor.
 *
 * Created by laverluisant.
 */
public enum TestDataTypeEnum {
    /**
     * Parses json TestData constant.
     */
    JSON(Constants.JSON) {
        @Override
        public String checkAndModifyValue(Field field)
                throws IllegalAccessException, InstantiationException, InvocationTargetException {
            try {
                String value = field.get(ClassUtils.getZeroParamsConstructor(field.getType()).newInstance()).toString();
                new JsonParser().parse(value);
                return value;
            }catch (JsonSyntaxException e){
                throw new WrongTestDataRuntimeException(e.getMessage());
            }
        }
    },
    /**
     *  Parses key/value TestData constant.
     */
    MAP(Constants.MAP) {
        @Override
        public String checkAndModifyValue(Field field)
                throws IllegalAccessException, InstantiationException, InvocationTargetException {
            return field.get(ClassUtils.getZeroParamsConstructor(field.getType()).newInstance()).toString();
        }
    },
    /**
     * Parses path params.
     */
    SPLITABLE_STRING(Constants.SPLITABLE_STRING) {
        @Override
        public String checkAndModifyValue(Field field)
                throws IllegalAccessException, InstantiationException, InvocationTargetException {
            try {
                UrlParamUtils.splitParamsToMap(ClassUtils.getZeroParamsConstructor(field.getType()).newInstance().toString());
                return field.get(ClassUtils.getZeroParamsConstructor(field.getType()).newInstance()).toString();
            } catch (CannotConvertResponseException e) {
                throw new WrongTestDataRuntimeException(e.getMessage());
            }
        }
    },

    HEADER(Constants.HEADER){
        @Override
        public Object checkAndModifyValue(Field field) throws IllegalAccessException, InstantiationException, InvocationTargetException {
            return field.get(ClassUtils.getZeroParamsConstructor(field.getType()).newInstance());
        }
    };

    private String type;

    TestDataTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    /**
     *
     * @param field static data from {@link Case} annotated class marked as {@link TestData}
     * @return Build json from input data follows type algorithm.
     * @throws IllegalAccessException Sneaky exception.
     * @throws InstantiationException Sneaky exception.
     * @throws InvocationTargetException Sneaky exception.
     */
    abstract public Object checkAndModifyValue(Field field)
            throws IllegalAccessException, InstantiationException, InvocationTargetException;

    /**
     * Constants to be used in annotation {@link TestData#type()}
     */
    public static class Constants {
        public static final String JSON = "JSON";
        public static final String MAP = "MAP";
        public static final String SPLITABLE_STRING = "SPLITABLE_STRING";
        public static final String HEADER = "HEADER";

        /**
         * Gets all Constants.
         *
         * @return {@link List} of all constants are contained in {@link TestDataTypeEnum.Constants}.
         */
        public static List<String> getAllConstants() {
            return Arrays.stream(TestDataTypeEnum.Constants.class.getFields())
                    .map(Field::getName)
                    .collect(Collectors.toList());
        }

        /**
         * {@link List} of constance to use with {@link MethodCase#testDataNameReferences()}.
         *
         * @return {@link List} {@literal <}{@link String}{@literal >}
         */
        public static List<String> getConstantsForNameReferences() {
            return getAllConstants().stream().filter(c -> !c.equals(HEADER)).collect(Collectors.toList());
        }

        /**
         * {@link List} of constance to use with {@link MethodCase#testDataRequestHeader()}.
         *
         * @return {@link List} {@literal <}{@link String}{@literal >}
         */
        public static List<String> getConstantForHeadersReferences() {
            return Arrays.asList(HEADER);
        }
    }
}
