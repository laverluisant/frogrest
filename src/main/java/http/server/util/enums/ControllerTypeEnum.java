package http.server.util.enums;

import http.server.handler.Handler;
import http.server.handler.impl.CrudEndpointHandlerEnum;
import http.server.handler.impl.RestEndpointHandlerEnum;
import http.server.util.BaseUtils;

/**
 * Small factory to create controller handler.
 */
public enum ControllerTypeEnum {
    MULTY {
        @Override
        public Handler getHandler(String verb) {
            RestEndpointHandlerEnum restHandler =
                    (RestEndpointHandlerEnum) BaseUtils.getValueByStringOrDefault(
                            verb,
                            RestEndpointHandlerEnum.DEFAULT,
                            RestEndpointHandlerEnum.class);
            if (restHandler == RestEndpointHandlerEnum.DEFAULT) {
                restHandler.setCurrentVerb(verb);
            }
            return restHandler;
        }
    },
    CRUD {
        @Override
        public Handler getHandler(String verb) {
            CrudEndpointHandlerEnum crudHandler =
                    (CrudEndpointHandlerEnum) BaseUtils.getValueByStringOrDefault(
                            verb,
                            CrudEndpointHandlerEnum.DEFAULT,
                            CrudEndpointHandlerEnum.class);
            if (crudHandler == CrudEndpointHandlerEnum.DEFAULT) {
                crudHandler.setCurrentVerb(verb);
            }
            return crudHandler;
        }
    };

    /**
     * @param verb key to reach controller point. If request method is {@link RestEndpointHandlerEnum.Constants#OPTIONS}
     *             when verb represent key-point to method annotated {@link http.server.annotation.Default}. Else it's
     *             equal to request http method.
     * @return certain instance of {@link Handler} represents by instance of {@link CrudEndpointHandlerEnum}
     * or {@link RestEndpointHandlerEnum}.
     */
    abstract public Handler getHandler(String verb);
}
