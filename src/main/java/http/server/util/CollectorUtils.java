package http.server.util;

import http.server.exception.runtime.DuplicateVerbRuntimeException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class CollectorUtils {

    public final static String DUPLICATION_ERROR_MESSAGE = "Incompatible values in list %.";

    /**
     * Convert list with one object type {@literal <}T{@literal >} to {@literal <}T{@literal >}
     *
     * @param <T>
     * @return first and single element of list.
     */
    public static <T> Collector<T, List<T>, T> toFirstListElement() {
        return Collector.of(
                ArrayList::new,
                List::add,
                (left, right) -> {
                    left.addAll(right);
                    return left;
                },
                list -> {
                    if (list.size() > 1) {
                        throw new DuplicateVerbRuntimeException(String.format(DUPLICATION_ERROR_MESSAGE, list.toString()));
                    }
                    if (list.size() == 0)
                        return null;
                    return list.get(0);
                }
        );
    }
}
