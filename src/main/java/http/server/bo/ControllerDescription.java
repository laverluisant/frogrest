package http.server.bo;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import http.server.util.enums.ControllerTypeEnum;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Bo which contains all necessary data to describe controller param for future handling.
 */
public class ControllerDescription {
    private Method defaultMethod;
    private EndpointBo endpointBo;
    private Map<String, MultiDescription> multiDescription;
    private ControllerTypeEnum controllerType;


    public Method getDefaultMethod() {
        return defaultMethod;
    }

    public void setDefaultMethod(Method defaultMethod) {
        this.defaultMethod = defaultMethod;
    }

    public EndpointBo getEndpointBo() {
        return endpointBo;
    }

    public void setEndpointBo(EndpointBo endpointBo) {
        this.endpointBo = endpointBo;
    }

    public Map<String, MultiDescription> getMethodDescription() {
        return multiDescription;
    }

    public void setMethodDescription(Map<String, MultiDescription> methodDescription) {
        this.multiDescription = methodDescription;
    }

    public ControllerTypeEnum getControllerType() {
        return controllerType;
    }

    public void setControllerType(ControllerTypeEnum controllerType) {
        this.controllerType = controllerType;
    }

    /**
     * Builder for {@link MultiDescription} to contain data about endpoint controller.
     * To build instance of {@link MultiDescription} use {@link MultiDescription.Builder#build()}.
     *
     * {@link MultiDescription.Builder#setCertainEndpointBo(EndpointBo)} set instance of {@link EndpointBo}
     * describing current method path variable (name and certain place).
     * {@link MultiDescription.Builder#setCertainClazz(Class)} set current type of class which is
     * annotated {@link http.server.annotation.rest.RestController}
     * {@link MultiDescription.Builder#setCertainMethod(Method)} set current method to be involved in
     * appropriate child of {@link http.server.handler.Handler}.
     * {@link MultiDescription.Builder#setResponseHeaders(Headers)} set headers to set in response
     * to {@link HttpExchange#getResponseHeaders()}.
     * {@link MultiDescription.Builder#setRequiredParams(List)} (Method)} set {@link List} of required params.
     * to {@link HttpExchange#getResponseHeaders()}.
     * {@link MultiDescription.Builder#setOnErrorMethod(Method)} set method for default exception handling.
     * to {@link HttpExchange#getResponseHeaders()}.
     *
     * @return instance of {@link MultiDescription.Builder}.
     *
     */
    public MultiDescription.Builder multiDescriptionBuilder() {
        return new MultiDescription().newBuilder();
    }

    /**
     * Inner class to define almost all data for multi endpoints description.
     */
    public class MultiDescription {
        private EndpointBo certainEndpointBo;
        private Class certainClazz;
        private Method certainMethod;
        private Headers responseHeaders;
        private List requiredParams;
        private Method onErrorMethod;
        private Method beforeMethod;
        private Method afterMethod;

        public MultiDescription() {
        }

        public EndpointBo getCertainEndpointBo() {
            return certainEndpointBo;
        }

        public Class getCertainClazz() {
            return certainClazz;
        }

        public Method getCertainMethod() {
            return certainMethod;
        }

        public Headers getResponseHeaders() {
            return responseHeaders;
        }

        public List getRequiredParams() {
            return requiredParams;
        }

        public Method getOnErrorMethod() {
            return onErrorMethod;
        }

        public Method getBeforeMethod() {
            return beforeMethod;
        }

        public Method getAfterMethod() {
            return afterMethod;
        }

        public Builder newBuilder() {
            return new MultiDescription().new Builder();
        }

        public class Builder {

            private Builder() {
            }

            public Builder setCertainEndpointBo(EndpointBo certainEndpointBo) {
                MultiDescription.this.certainEndpointBo = certainEndpointBo;
                return this;
            }

            public Builder setCertainClazz(Class certainClazz) {
                MultiDescription.this.certainClazz = certainClazz;
                return this;
            }

            public Builder setCertainMethod(Method certainMethod) {
                MultiDescription.this.certainMethod = certainMethod;
                return this;
            }

            public Builder setResponseHeaders(Headers responseHeaders) {
                MultiDescription.this.responseHeaders = responseHeaders;
                return this;
            }

            public Builder setRequiredParams(List requiredParams) {
                MultiDescription.this.requiredParams = requiredParams;
                return this;
            }

            public Builder setOnErrorMethod(Method onErrorMethod) {
                MultiDescription.this.onErrorMethod = onErrorMethod;
                return this;
            }

            public Builder setAfterMethod(Method afterMethod) {
                MultiDescription.this.afterMethod = afterMethod;
                return this;
            }

            public Builder setBeforeMethod(Method beforeMethod) {
                MultiDescription.this.beforeMethod = beforeMethod;
                return this;
            }

            /**
             * Build {@link Builder} to {@link MultiDescription} object.
             *
             * @return instance of {@link MultiDescription} with info about combination current instance of {@link EndpointBo}
             * and method verb.
             */
            public MultiDescription build() {
                return MultiDescription.this;
            }
        }
    }
}
