package http.server.bo;

/**
 * Created by laverluisant on 01.11.17.
 *
 * Describes income param of method.
 */
public class FieldDescriptionBo {
    private Class clazz;
    private boolean isRequered;

    public FieldDescriptionBo(Class clazz, boolean isRequered) {
        this.clazz = clazz;
        this.isRequered = isRequered;
    }

    public Class getClazz() {
        return clazz;
    }

    public boolean isRequered() {
        return isRequered;
    }
}
