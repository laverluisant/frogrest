package http.server.bo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Describe endpoint properties and param.
 *
 * Created by laverluisant on 01.06.17.
 */
public final class EndpointBo {
    private final List fullKeys;

    private final Set pathKeys;

    /**
     * @param fullKeys {@link List} of all parts of endpoint
     * @param pathKeys {@link List} of path parts
     */
    public EndpointBo(List<String> fullKeys, Set<Integer> pathKeys) {
        this.fullKeys = Collections.unmodifiableList(fullKeys);
        this.pathKeys = Collections.unmodifiableSet(pathKeys);
    }

    public List getFullKeys() {
        return fullKeys;
    }

    public Set getPathKeys() {
        return pathKeys;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof EndpointBo))
            return false;
        List thatList = ((EndpointBo) o).getFullKeys();
        if (thatList.size() != fullKeys.size())
            return false;
        List filteredThat = new ArrayList();
        List filteredThis = new ArrayList();
        for (int i = 0; i < thatList.size(); i++) {
            if (!((EndpointBo) o).getPathKeys().contains(i)) {
                filteredThat.add(thatList.get(i));
                filteredThis.add(getFullKeys().get(i));
            }
        }
        if (filteredThat.size() == filteredThis.size()) {
            for (int i = 0; i < filteredThat.size(); i++) {
                if (!filteredThat.get(i).equals(filteredThis.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 31 * fullKeys.size();
    }
}
