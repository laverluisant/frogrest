package http.server.exception.runtime;

/**
 * Created by laverluisant on 17.05.17.
 */
public class DuplicateEndpointRuntimeException extends RuntimeException {
    public String message;

    public DuplicateEndpointRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
