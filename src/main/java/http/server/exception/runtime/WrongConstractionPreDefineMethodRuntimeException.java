package http.server.exception.runtime;

public class WrongConstractionPreDefineMethodRuntimeException extends RuntimeException {
    public WrongConstractionPreDefineMethodRuntimeException(String message) {
        super(message);
    }
}
