package http.server.exception.runtime;

public class CRUDMissedDefaultPointMethodRuntimeException extends RuntimeException {
    public CRUDMissedDefaultPointMethodRuntimeException(String message) {
        super(message);
    }
}
