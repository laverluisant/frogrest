package http.server.exception.runtime;

public class DuplicationPredefinedMethodAnnotationRuntimeException extends RuntimeException {
    public DuplicationPredefinedMethodAnnotationRuntimeException(String message) {
        super(message);
    }
}
