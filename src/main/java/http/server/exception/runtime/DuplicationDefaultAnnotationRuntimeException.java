package http.server.exception.runtime;

/**
 * Created by laverluisant on 4.11.17.
 */
public class DuplicationDefaultAnnotationRuntimeException extends  RuntimeException{
    private String message;

    public DuplicationDefaultAnnotationRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
