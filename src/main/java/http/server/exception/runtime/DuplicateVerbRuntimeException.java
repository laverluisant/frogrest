package http.server.exception.runtime;

/**
 * Created by laverluisant on 26.10.17.
 */
public class DuplicateVerbRuntimeException extends RuntimeException {
    private String message;

    public DuplicateVerbRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}