package http.server.exception.runtime;

public class UnsupportedEncodingRuntimeException extends RuntimeException{
    public UnsupportedEncodingRuntimeException(String message) {
        super(message);
    }
}
