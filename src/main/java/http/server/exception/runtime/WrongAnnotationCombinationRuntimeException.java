package http.server.exception.runtime;

/**
 * Created by laverluisant on 17.05.17.
 */
public class WrongAnnotationCombinationRuntimeException extends RuntimeException {
    public String message;

    public WrongAnnotationCombinationRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}