package http.server.exception.runtime;

/**
 * Created by laverluisant on 29.10.17.
 */
public class DuplicateAnnotedNamesInMethodRuntimeExcetion extends RuntimeException {
    public String message;

    public DuplicateAnnotedNamesInMethodRuntimeExcetion(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}