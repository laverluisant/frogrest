package http.server.exception.runtime;

public class WrongTestDataRuntimeException extends RuntimeException {
    private String message;

    public WrongTestDataRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
