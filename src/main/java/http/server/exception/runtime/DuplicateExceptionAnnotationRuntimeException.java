package http.server.exception.runtime;

/**
 * Created by laverluisant on 29.05.17.
 */
public class DuplicateExceptionAnnotationRuntimeException extends RuntimeException {
    public String message;

    public DuplicateExceptionAnnotationRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
