package http.server.exception.runtime;

/**
 * Created by laverluisant on 17.05.17.
 */
public class InvalidEndpointRuntimeException extends RuntimeException {
    public String message;

    public InvalidEndpointRuntimeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
