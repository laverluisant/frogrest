package http.server.exception.runtime;

public class NoZeroParamsConstructorExistRuntimeException extends RuntimeException{
    public NoZeroParamsConstructorExistRuntimeException(String message) {
        super(message);
    }
}
