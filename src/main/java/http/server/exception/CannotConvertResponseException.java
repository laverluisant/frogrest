package http.server.exception;

/**
 * Created by laverluisant on 24.05.17.
 */
public class CannotConvertResponseException extends Exception {
    public String message;

    public CannotConvertResponseException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
