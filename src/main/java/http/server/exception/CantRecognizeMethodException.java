package http.server.exception;

/**
 *  Created by laverluisant on 15.10.17.
 */
public class CantRecognizeMethodException extends Exception {
    public String message;

    public CantRecognizeMethodException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
