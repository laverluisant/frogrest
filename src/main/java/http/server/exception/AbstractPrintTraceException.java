package http.server.exception;

import java.util.Arrays;

/**
 * Abstract class to realize logic of printing trace exception.
 *
 * Created by laverluisant.
 */
public abstract class AbstractPrintTraceException extends Exception {

    private String message;
    private String debugMessage;

    public AbstractPrintTraceException() {
    }

    public AbstractPrintTraceException(String message) {
        this.message = message;
        setStackTrace(Thread.currentThread().getStackTrace());
    }

    public AbstractPrintTraceException(String message, String debugMessage) {
        this.message = message;
        this.debugMessage = debugMessage;
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return super.getStackTrace();
    }

    @Override
    public void setStackTrace(StackTraceElement[] stackTrace) {
        super.setStackTrace(stackTrace);
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getDebugMessage() {
        return debugMessage;
    }

    @Override
    public String toString() {
        return "AbstractPrintTraceException{" +
                "message='" + message + '\'' +
                ", debugMessage='" + debugMessage + '\'' +
                '}';
    }
}
