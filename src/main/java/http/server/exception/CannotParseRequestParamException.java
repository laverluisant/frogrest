package http.server.exception;

/**
 * Created by laverluisant on 26.05.17.
 */
public class CannotParseRequestParamException extends Exception {
    public String message;

    public CannotParseRequestParamException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
