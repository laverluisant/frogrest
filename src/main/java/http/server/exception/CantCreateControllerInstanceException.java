package http.server.exception;

public class CantCreateControllerInstanceException extends Exception {
    public CantCreateControllerInstanceException(String message) {
        super(message);
    }
}
