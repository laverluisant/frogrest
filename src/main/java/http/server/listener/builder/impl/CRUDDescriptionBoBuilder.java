package http.server.listener.builder.impl;

import com.sun.net.httpserver.Headers;
import http.server.annotation.Default;
import http.server.annotation.ResponseHeaders;
import http.server.annotation.crud.CRUDController;
import http.server.annotation.crud.CustomPoint;
import http.server.annotation.crud.RequiredFields;
import http.server.bo.ControllerDescription;
import http.server.bo.EndpointBo;
import http.server.exception.runtime.CRUDMissedDefaultPointMethodRuntimeException;
import http.server.exception.runtime.DuplicateEndpointRuntimeException;
import http.server.exception.runtime.DuplicateVerbRuntimeException;
import http.server.exception.runtime.InvalidEndpointRuntimeException;
import http.server.handler.impl.RestEndpointHandlerEnum;
import http.server.listener.DefaultHTTPListener;
import http.server.listener.builder.Builder;
import http.server.util.EndpointUtils;
import http.server.util.enums.ControllerTypeEnum;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collector;

/**
 * Build instance {@link ControllerDescription} for crud controller and add it to {@link DefaultHTTPListener#getControllers()}.
 * Can create only one endpoint for controller follows CRUD conceptions.
 * <p>
 * Method can be annotated as {@link http.server.annotation.crud.Get}, {@link http.server.annotation.crud.Post},
 * {@link http.server.annotation.crud.Put}, {@link http.server.annotation.crud.Delete} and would be recognized as
 * default rest verb-methods. Controller should contains all default point-method or would be thrown
 * {@link CRUDMissedDefaultPointMethodRuntimeException}. Or as custom verb with annotation {@link CustomPoint} -
 * can be used only english letter or would be thrown {@link InvalidEndpointRuntimeException}.
 * <p>
 * If current verb already exists for this endpoint would be thrown {@link DuplicateVerbRuntimeException}.
 * If {@link DefaultHTTPListener#getControllers()} already contains current endpoint key
 * would be thrown {@link DuplicateEndpointRuntimeException}.
 */
public class CRUDDescriptionBoBuilder implements Builder {
    private Class clazz;
    private Map<EndpointBo, ControllerDescription> controllers = new HashMap<>();
    private String controllerEndpoint;
    private EndpointBo currentEndpointBo;
    private Method onErrorMethod;
    private Method beforeMethod;
    private Method afterMethod;

    public CRUDDescriptionBoBuilder(Class clazz, Map<EndpointBo, ControllerDescription> controllers) {
        this.clazz = clazz;
        this.controllers = controllers;
    }


    @Override
    public Method getOnErrorMethod() {
        return onErrorMethod;
    }

    @Override
    public void setOnErrorMethod(Method onErrorMethod) {
        this.onErrorMethod = onErrorMethod;
    }

    @Override
    public Method getBeforeMethod() {
        return beforeMethod;
    }

    @Override
    public void setBeforeMethod(Method beforeMethod) {
        this.beforeMethod = beforeMethod;
    }

    @Override
    public Method getAfterMethod() {
        return afterMethod;
    }

    @Override
    public void setAfterMethod(Method afterMethod) {
        this.afterMethod = afterMethod;
    }

    @Override
    public Class getClazz() {
        return clazz;
    }

    @Override
    public void build() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        CRUDController restController = (CRUDController) clazz.getAnnotation(CRUDController.class);
        controllerEndpoint = EndpointUtils.replace(restController.endpoint());
        currentEndpointBo = EndpointUtils.createEndpoint(controllerEndpoint, clazz);
        if (controllers.containsKey(currentEndpointBo)) {
            throw new DuplicateEndpointRuntimeException(DUPLICATION_ANNOTATION_MESSAGE);
        }
        setPredefineMethods();
        Arrays.stream(clazz.getMethods())
                .filter(this::isCrudEndpoint)
                .forEach(this::upsertControllers);
        RestEndpointHandlerEnum.Constants.getDefaultCrudVerbs()
                .forEach(v -> {
                    if (!controllers.get(currentEndpointBo).getMethodDescription().keySet().contains(v))
                        throw new CRUDMissedDefaultPointMethodRuntimeException(
                                String.format(CRUD_MISSED_DEFAULT_METHOD_MESSAGE, v, this.getClass().getName()));
                });
    }

    private boolean isCrudEndpoint(Method candidate) {
        candidate.isAnnotationPresent(CustomPoint.class);
        return Arrays.stream(candidate.getAnnotations())
                .anyMatch(this::isCrudMethodAnnotation);
    }

    private boolean isCrudMethodAnnotation(Annotation annotation) {
        return RestEndpointHandlerEnum.Constants.getAllConstants()
                .contains(annotation.annotationType().getSimpleName().toUpperCase())
                || annotation instanceof CustomPoint;
    }

    private void upsertControllers(Method method) {
        Headers responseHeaders = new Headers();
        if (method.isAnnotationPresent(ResponseHeaders.class))
            responseHeaders = Arrays.stream(method.getAnnotation(ResponseHeaders.class).headers())
                    .collect(Collector.of(
                            Headers::new,
                            (headers, header) -> headers.put(header.name(), Arrays.asList(header.value())),
                            (left, right) -> {
                                left.putAll(right);
                                return left;
                            })
                    );
        Annotation crud = EndpointUtils.findCRUDAnnotation(method.getAnnotations());
        CustomPoint customCrud = null;
        if (crud == null) {
            customCrud = method.getAnnotation(CustomPoint.class);
        }
        ControllerDescription description;
        RequiredFields rfAnnotation = method.getAnnotation(RequiredFields.class);
        List<String> requiredFields;
        if (rfAnnotation != null) {
            requiredFields = Arrays.asList(method.getAnnotation(RequiredFields.class).fields());
        } else {
            requiredFields = new ArrayList<>();
        }
        if (controllers.containsKey(currentEndpointBo)) {
            description = controllers.get(currentEndpointBo);
            if (method.isAnnotationPresent(Default.class)) {
                if (description.getDefaultMethod() != null)
                    throw new DuplicateEndpointRuntimeException(
                            String.format(DUPLICATION_DEFAULT_ANNOTATION_MESSAGE,
                                    description.getDefaultMethod().getName(),
                                    method.getName()));
                else description.setDefaultMethod(method);
            }
            Set<String> verbs = description.getMethodDescription().keySet();
            if (isVerbAlreadyExist(verbs, crud, customCrud)) {
                throw new DuplicateVerbRuntimeException(String.format(
                        DUPLICATION_ENDPOINT_MESSAGE, controllerEndpoint, clazz.getName()));
            }
            if (!EndpointUtils.isValidEndpointVerb(crud != null ? crud.annotationType().getSimpleName().toUpperCase() : customCrud.verb())) {
                throw new InvalidEndpointRuntimeException(
                        String.format(INVALID_VERB_MESSAGE,
                                crud != null ? crud.annotationType().getSimpleName().toUpperCase() : customCrud.verb(),
                                crud != null ? crud.annotationType().getSimpleName().toUpperCase() : customCrud.getClass().getName(),
                                method.getName()));
            }
            description.getMethodDescription().put(
                    crud != null ? crud.annotationType().getSimpleName().toUpperCase() : customCrud.verb().toUpperCase(),
                    description.multiDescriptionBuilder()
                            .setCertainEndpointBo(currentEndpointBo)
                            .setCertainClazz(clazz)
                            .setCertainMethod(method)
                            .setResponseHeaders(responseHeaders)
                            .setRequiredParams(requiredFields)
                            .setOnErrorMethod(onErrorMethod)
                            .setBeforeMethod(beforeMethod)
                            .setAfterMethod(afterMethod)
                            .build());
            controllers.put(currentEndpointBo, description);
        } else {
            description = new ControllerDescription();
            description.setMethodDescription(new HashMap<>());
            description.getMethodDescription().put(
                    crud != null ? crud.annotationType().getSimpleName().toUpperCase() : customCrud.verb().toUpperCase(),
                    description.multiDescriptionBuilder()
                            .setCertainEndpointBo(currentEndpointBo)
                            .setCertainClazz(clazz)
                            .setCertainMethod(method)
                            .setResponseHeaders(responseHeaders)
                            .setRequiredParams(requiredFields)
                            .setOnErrorMethod(onErrorMethod)
                            .setBeforeMethod(beforeMethod)
                            .setAfterMethod(afterMethod)
                            .build());
            description.setEndpointBo(currentEndpointBo);
            description.setControllerType(ControllerTypeEnum.CRUD);
            controllers.put(currentEndpointBo, description);
        }
    }

    private boolean isVerbAlreadyExist(Set<String> verbs, Annotation crud, CustomPoint customCrud) {
        return crud != null && verbs.contains(crud.annotationType().getSimpleName().toUpperCase())
                || crud == null && verbs.contains(customCrud.verb());
    }
}