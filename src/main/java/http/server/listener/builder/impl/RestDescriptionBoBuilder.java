package http.server.listener.builder.impl;

import com.sun.net.httpserver.Headers;
import http.server.annotation.Default;
import http.server.annotation.ResponseHeaders;
import http.server.annotation.rest.RestController;
import http.server.annotation.rest.RestPoint;
import http.server.bo.ControllerDescription;
import http.server.bo.EndpointBo;
import http.server.exception.runtime.DuplicateEndpointRuntimeException;
import http.server.exception.runtime.DuplicateVerbRuntimeException;
import http.server.exception.runtime.InvalidEndpointRuntimeException;
import http.server.listener.DefaultHTTPListener;
import http.server.listener.builder.Builder;
import http.server.util.EndpointUtils;
import http.server.util.enums.ControllerTypeEnum;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collector;

/**
 * Build instance of {@link ControllerDescription} for multi endpoint controllers and add it to {@link DefaultHTTPListener#getControllers()}.
 * Can create no, one or several endpoints depends on
 * if {@link DefaultHTTPListener#getControllers()} already contains instance of {@link EndpointBo}
 * as a key or not. And if method with {@link RestPoint#endpoint()} define one of more endpoints.
 * <p>
 * {@link DefaultHTTPListener#getControllers()} can not contain together crud and multi-endpoint method at one
 * {@link EndpointBo} key. So if {@link DefaultHTTPListener#getControllers()} has {@link ControllerDescription#getControllerType()} ()} is
 * {@link ControllerTypeEnum#CRUD}
 * would be thrown {@link DuplicateEndpointRuntimeException}.
 */
public class RestDescriptionBoBuilder implements Builder {
    private Class clazz;
    private Map<EndpointBo, ControllerDescription> controllers = new HashMap<>();
    private String controllerEndpoint;
    private Method onErrorMethod;
    private Method beforeMethod;
    private Method afterMethod;

    public RestDescriptionBoBuilder(Class clazz, Map<EndpointBo, ControllerDescription> controllers) {
        this.clazz = clazz;
        this.controllers = controllers;
    }

    @Override
    public Method getOnErrorMethod() {
        return onErrorMethod;
    }

    @Override
    public void setOnErrorMethod(Method onErrorMethod) {
        this.onErrorMethod = onErrorMethod;
    }

    @Override
    public Method getBeforeMethod() {
        return beforeMethod;
    }

    @Override
    public void setBeforeMethod(Method beforeMethod) {
        this.beforeMethod = beforeMethod;
    }

    @Override
    public Method getAfterMethod() {
        return afterMethod;
    }

    @Override
    public void setAfterMethod(Method afterMethod) {
        this.afterMethod = afterMethod;
    }

    @Override
    public Class getClazz() {
        return clazz;
    }

    @Override
    public void build()
            throws IllegalAccessException, InvocationTargetException, InstantiationException {
        RestController restController = (RestController) clazz.getAnnotation(RestController.class);
        controllerEndpoint = EndpointUtils.replace(restController.endpoint());
        setPredefineMethods();
        Arrays.stream(clazz.getMethods())
                .filter(method -> method.isAnnotationPresent(RestPoint.class))
                .forEach(this::upsertControllers);
    }

    private void upsertControllers(Method method) {
        Headers responseHeaders = new Headers();
        if (method.isAnnotationPresent(ResponseHeaders.class))
            responseHeaders = Arrays.stream(method.getAnnotation(ResponseHeaders.class).headers())
                    .collect(Collector.of(
                            Headers::new,
                            (headers, header) -> headers.put(header.name(), Arrays.asList(header.value())),
                            (left, right) -> {
                                left.putAll(right);
                                return left;
                            })
                    );
        RestPoint rest = method.getAnnotation(RestPoint.class);
        EndpointBo currentEndpointBo = EndpointUtils.createEndpoint(
                controllerEndpoint + "/" + rest.endpoint(), clazz);
        ControllerDescription description;
        if (controllers.containsKey(currentEndpointBo)) {
            description = controllers.get(currentEndpointBo);
            if (description.getControllerType() == ControllerTypeEnum.CRUD) {
                throw new DuplicateEndpointRuntimeException(DUPLICATION_ANNOTATION_MESSAGE);
            }
            if (method.isAnnotationPresent(Default.class)) {
                if (description.getDefaultMethod() != null)
                    throw new DuplicateEndpointRuntimeException(
                            String.format(DUPLICATION_DEFAULT_ANNOTATION_MESSAGE,
                                    description.getDefaultMethod().getName(),
                                    method.getName()));
                else description.setDefaultMethod(method);
            }
            Set<String> verbs = description.getMethodDescription().keySet();
            if (verbs.contains(rest.verb())) {
                throw new DuplicateVerbRuntimeException(String.format(
                        DUPLICATION_ENDPOINT_MESSAGE, controllerEndpoint + rest.endpoint(), clazz.getName()));
            }
            if (!EndpointUtils.isValidEndpointVerb(rest.verb())) {
                throw new InvalidEndpointRuntimeException(
                        String.format(INVALID_VERB_MESSAGE,
                                rest.verb().toUpperCase(), RestPoint.class.getName(), method.getName()));
            }
            EndpointBo mainEndpointBo = new EndpointBo(
                    description.getEndpointBo().getFullKeys(),
                    new HashSet() {{
                        addAll(description.getEndpointBo().getPathKeys());
                        addAll(currentEndpointBo.getPathKeys());
                    }});
            description.getMethodDescription().put(
                    rest.verb(),
                    description.multiDescriptionBuilder()
                            .setCertainEndpointBo(currentEndpointBo)
                            .setCertainClazz(clazz)
                            .setCertainMethod(method)
                            .setResponseHeaders(responseHeaders)
                            .setRequiredParams(null)
                            .setOnErrorMethod(onErrorMethod)
                            .setBeforeMethod(beforeMethod)
                            .setAfterMethod(afterMethod)
                            .build());
            controllers.remove(mainEndpointBo);// !important don't change old and new key are the same so doesn't rewrite key
            controllers.put(mainEndpointBo, description);
        } else {
            description = new ControllerDescription();
            description.setMethodDescription(new HashMap<>());
            description.getMethodDescription().put(
                    rest.verb(),
                    description.multiDescriptionBuilder()
                            .setCertainEndpointBo(currentEndpointBo)
                            .setCertainClazz(clazz)
                            .setCertainMethod(method)
                            .setResponseHeaders(responseHeaders)
                            .setRequiredParams(null)
                            .setOnErrorMethod(onErrorMethod)
                            .setBeforeMethod(beforeMethod)
                            .setAfterMethod(afterMethod)
                            .build());
            description.setControllerType(ControllerTypeEnum.MULTY);
            description.setEndpointBo(currentEndpointBo);
            controllers.put(currentEndpointBo, description);
        }
    }
}
