package http.server.listener.builder;

import http.server.annotation.After;
import http.server.annotation.Before;
import http.server.annotation.OnError;
import http.server.bo.ControllerDescription;
import http.server.exception.runtime.DuplicationPredefinedMethodAnnotationRuntimeException;
import http.server.exception.runtime.WrongConstractionPreDefineMethodRuntimeException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Builds instance of {@link ControllerDescription} to store all controller info.
 */
public interface Builder {

    String CRUD_MISSED_DEFAULT_METHOD_MESSAGE = "CRUD controller should contains all default method. %s was missed in %s.class.";
    String DUPLICATION_ANNOTATION_MESSAGE = "Could not have the same verb type annotation in one controller annotated @CRUD";
    String DUPLICATION_DEFAULT_ANNOTATION_MESSAGE = "Couldn't be twice @Default annotation in %s and %s.";
    String DUPLICATION_ENDPOINT_MESSAGE = "Duplicate endpoint %s in %s.class.";
    String INVALID_VERB_MESSAGE = "Invalid verb %s at annotation %s in %s(...) ";
    String NON_VOID_METHOD_MESSAGE = "Predefine method couldn't return anything. %() should be void.";
    String SHOULDNT_CONTAINS_ANY_INCOME_PARAMS_MESSAGE = "Method shouldn't contains any income parameters. Wrong construction at %s(...)";
    String DUPLICATION_PREDEFINED_ANNOTATION_MESSAGE = "Only one method at class can be marked as @%s. Error at %s.class#%s(...).";

    void build() throws IllegalAccessException, InvocationTargetException, InstantiationException;

    Method getOnErrorMethod();

    void setOnErrorMethod(Method onErrorMethod);

    Method getBeforeMethod();

    void setBeforeMethod(Method beforeMethod);

    Method getAfterMethod();

    void setAfterMethod(Method afterMethod);

    Class getClazz();

    /**
     * Set predefine methods with annotations: {@link OnError}, {@link Before} and {@link After}.
     * Annotated method should be void(attitude only to {@link Before} and {@link After} annotations)
     * and has no params or would be thrown {@link WrongConstractionPreDefineMethodRuntimeException}.
     */
    default void setPredefineMethods() {
        Arrays.stream(getClazz().getMethods()).forEach(m -> {
            if (m.isAnnotationPresent(OnError.class)) {
                if (getOnErrorMethod() != null) {
                    throw new DuplicationPredefinedMethodAnnotationRuntimeException(
                            String.format(DUPLICATION_PREDEFINED_ANNOTATION_MESSAGE,
                                    OnError.class.getSimpleName(),
                                    m.getDeclaringClass().getSimpleName(),
                                    m.getName())
                    );
                }
                if (m.getParameterCount() != 0)
                    throw new WrongConstractionPreDefineMethodRuntimeException(
                            String.format(SHOULDNT_CONTAINS_ANY_INCOME_PARAMS_MESSAGE, m.getName()));
                setOnErrorMethod(m);
            } else if (m.isAnnotationPresent(Before.class)) {
                if (getBeforeMethod() != null) {
                    throw new DuplicationPredefinedMethodAnnotationRuntimeException(
                            String.format(DUPLICATION_PREDEFINED_ANNOTATION_MESSAGE,
                                    Before.class.getSimpleName(),
                                    m.getDeclaringClass().getSimpleName(),
                                    m.getName())
                    );
                }
                checkMethodConstruction(m);
                setBeforeMethod(m);
            } else if (m.isAnnotationPresent(After.class)) {
                if (getAfterMethod() != null) {
                    throw new DuplicationPredefinedMethodAnnotationRuntimeException(
                            String.format(DUPLICATION_PREDEFINED_ANNOTATION_MESSAGE,
                                    After.class.getSimpleName(),
                                    m.getDeclaringClass().getSimpleName(),
                                    m.getName())
                    );
                }
                checkMethodConstruction(m);
                setAfterMethod(m);
            }
        });
    }

    private void checkMethodConstruction(Method method) {
        if (!method.getReturnType().equals(Void.TYPE))
            throw new WrongConstractionPreDefineMethodRuntimeException(
                    String.format(NON_VOID_METHOD_MESSAGE, method.getName()));
        if (method.getParameterCount() != 0)
            throw new WrongConstractionPreDefineMethodRuntimeException(
                    String.format(SHOULDNT_CONTAINS_ANY_INCOME_PARAMS_MESSAGE, method.getName()));
    }

}
