package http.server.listener;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import http.server.annotation.ExceptionHandler;
import http.server.annotation.crud.CRUDController;
import http.server.annotation.rest.RestController;
import http.server.bo.ControllerDescription;
import http.server.bo.EndpointBo;
import http.server.exception.*;
import http.server.exception.runtime.DuplicateExceptionAnnotationRuntimeException;
import http.server.exception.runtime.NoZeroParamsConstructorExistRuntimeException;
import http.server.handler.Handler;
import http.server.handler.impl.RestEndpointHandlerEnum;
import http.server.listener.builder.impl.CRUDDescriptionBoBuilder;
import http.server.listener.builder.impl.RestDescriptionBoBuilder;
import http.server.util.BaseUtils;
import http.server.util.ClassUtils;
import http.server.util.EndpointUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Listener of http request.
 * <p>
 * Created by laverluisant on 17.05.17.
 */
public class DefaultHTTPListener implements HttpHandler {
    private final Map<EndpointBo, ControllerDescription> controllers;
    public final static String WRONG_COMBINATION_EHANDLER_MESSAGE =
            "Wrong combination annotation @ExceptionHandler with param isPrintStackTrace = true and not E extend AbstractPrintTraceException!";
    private final static String CANT_CREATE_CONTROLLER_INSTANCE_ERROR_MESSAGE = "Can't create controller instance. Url is unreachable. ";
    private final static String CANT_RECOGNIZE_REQUEST_METHOD_ERROR_MESSAGE = "Can't recognize request method.";

    /**
     * @param packages {@link List} of scanning packages.
     */
    public DefaultHTTPListener(List<String> packages) {
        controllers = new HashMap<>();
        try {
            for (Class clazz : ClassUtils.getClasses(this.getClass().getClassLoader(), packages)) {
                try {
                    if (clazz.isAnnotationPresent(RestController.class)) {
                        if (!ClassUtils.isZeroParamsConstructorExist(clazz)) {
                            throw new NoZeroParamsConstructorExistRuntimeException(
                                    String.format(ClassUtils.NO_ZERO_CONSTRUCTOR_MESSAGE, clazz.getName(), RestController.class.getName())
                            );
                        }
                        checkUniqueExceptionAnnotation(clazz);
                        RestDescriptionBoBuilder builder = new RestDescriptionBoBuilder(clazz, controllers);
                        builder.build();
                    } else if (clazz.isAnnotationPresent(CRUDController.class)) {
                        if (!ClassUtils.isZeroParamsConstructorExist(clazz)) {
                            throw new NoZeroParamsConstructorExistRuntimeException(
                                    String.format(ClassUtils.NO_ZERO_CONSTRUCTOR_MESSAGE, clazz.getName(), CRUDController.class.getName())
                            );
                        }
                        checkUniqueExceptionAnnotation(clazz);
                        CRUDDescriptionBoBuilder builder = new CRUDDescriptionBoBuilder(clazz, controllers);
                        builder.build();
                    }
                } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void checkUniqueExceptionAnnotation(Class controller) {
        Method[] methods = controller.getMethods();
        Set<Class<? extends Exception>> exceptions = new HashSet<>();
        for (Method method : methods) {
            ExceptionHandler eHandler = method.getAnnotation(ExceptionHandler.class);
            if (eHandler != null) {
                boolean isContains = exceptions
                        .stream()
                        .anyMatch(new HashSet(Arrays.asList(eHandler.exceptions()))::contains);
                if (isContains) {
                    throw new DuplicateExceptionAnnotationRuntimeException(
                            "Duplicate ExceptionHandler annotation in method " + method.getName() + "()");
                } else {
                    exceptions.addAll(Arrays.asList(eHandler.exceptions()));
                }
            }
        }
    }

    /**
     * Return {@link Map} of controllers where key is {@link EndpointBo} object and value is {@link ControllerDescription} object.
     *
     * @return {@link Map} of GET controllers
     */
    public Map<EndpointBo, ControllerDescription> getControllers() {
        return controllers;
    }

    private Object buildResponseBody(Object value) {
        if (value instanceof String)
            return value;
        return new Gson().toJson(value);
    }

    public void handle(HttpExchange exchange) throws IOException {
        String method = exchange.getRequestMethod();
        String endpoint = exchange.getRequestURI().getPath();
        byte[] bytes = new byte[0];
        EndpointBo endpointBo = EndpointUtils.splitPath(endpoint);
        if (controllers.containsKey(endpointBo)) {
            Object currentObj = null;
            String currentVerb = null;
            try {
                currentVerb = getVerb(method, endpointBo);
                currentObj = createControllerInstance(endpointBo, currentVerb);
                Handler handler = controllers.get(endpointBo).getControllerType().getHandler(currentVerb);
                if (handler != null) {
                    handler.setEndpoints(controllers.keySet());
                    bytes = handler.handle(exchange, endpointBo, controllers.get(endpointBo), currentObj);
                    exchange.sendResponseHeaders(200, bytes.length);
                } else {
                    throw new CantRecognizeMethodException(CANT_RECOGNIZE_REQUEST_METHOD_ERROR_MESSAGE);
                }
            } catch (ComingSoonException | CannotConvertResponseException |
                    CantRecognizeMethodException | CantCreateControllerInstanceException e) {
                Object obj = onErrorInvoke(currentObj, endpointBo, currentVerb);
                bytes = buildResponseBody(obj + e.getMessage()).toString().getBytes();
                exchange.sendResponseHeaders(501, bytes.length);
            } catch (AbsentRequiredParamsException e) {
                bytes = buildResponseBody(onErrorInvoke(currentObj, endpointBo, currentVerb)).toString().getBytes();
                exchange.sendResponseHeaders(400, bytes.length);
            } catch (NullPointerException e) {
                bytes = buildResponseBody(onErrorInvoke(currentObj, endpointBo, currentVerb)).toString().getBytes();
                exchange.sendResponseHeaders(500, bytes.length);
            } catch (Exception e) {
                bytes = exceptionHandler(endpointBo, currentVerb, e, currentObj, exchange);
            }
        } else {
            exchange.sendResponseHeaders(404, bytes.length);
        }
        OutputStream os = exchange.getResponseBody();
        os.write(bytes);
        os.close();
    }

    private Object onErrorInvoke(Object currentObj, EndpointBo endpointBo, String verb) {
        Object returnedObj = null;
        Method onErrorMethod = controllers.get(endpointBo).getMethodDescription().get(verb).getOnErrorMethod();
        if (currentObj != null && onErrorMethod != null) {
            try {
                if (onErrorMethod.getReturnType().equals(Void.TYPE)) {
                    onErrorMethod.invoke(currentObj);
                    returnedObj = new Object();
                } else {
                    returnedObj = onErrorMethod.invoke(currentObj);
                }
            } catch (IllegalAccessException | InvocationTargetException e1) {
            }
        } else
            returnedObj = BaseUtils.onError();
        return returnedObj;
    }

    private Object createControllerInstance(EndpointBo endpoint, String currentVerb) throws CantCreateControllerInstanceException {
        try {
            return ClassUtils.getZeroParamsConstructor(
                    controllers.get(endpoint).getMethodDescription().get(currentVerb).getCertainClazz()).newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new CantCreateControllerInstanceException(CANT_CREATE_CONTROLLER_INSTANCE_ERROR_MESSAGE);
        }
    }

    private String getVerb(String method, EndpointBo endpointBo) {
        if (method.equals(RestEndpointHandlerEnum.Constants.OPTIONS)) {
            return EndpointUtils.findCRUDAnnotation(controllers.get(endpointBo).getDefaultMethod().getAnnotations())
                    .annotationType().getSimpleName().toUpperCase();
        }
        return method;
    }

    private byte[] exceptionHandler(EndpointBo endpointBo, String currentVerb, Exception e, Object currentObj, HttpExchange exchange) throws IOException {
        Method[] methods = controllers.get(endpointBo).getMethodDescription().get(currentVerb).getCertainClazz().getMethods();
        byte[] bytes = new byte[0];
        boolean isCustomExceptionHandlingPresent = false;
        for (Method method2 : methods) {
            ExceptionHandler eHandler = method2.getAnnotation(ExceptionHandler.class);
            if (eHandler != null && Arrays.stream(eHandler.exceptions()).anyMatch(h -> e.getCause().getClass() == h)) {
                isCustomExceptionHandlingPresent = true;
                try {
                    if (method2.getReturnType().equals(Void.TYPE)) {
                        method2.invoke(controllers.get(endpointBo).getMethodDescription().get(currentVerb).getCertainClazz());
                        bytes = new byte[0];
                    } else {
                        if (eHandler.isPrintStackTrace()) {
                            String returnedObject =
                                    buildResponseBody(method2.invoke(
                                            ClassUtils.getZeroParamsConstructor(
                                                    controllers.get(endpointBo).getMethodDescription().get(currentVerb).getCertainClazz()).newInstance()))
                                            .toString();
                            if (AbstractPrintTraceException.class.isAssignableFrom(e.getCause().getClass())) {
                                if(e instanceof InvocationTargetException){
                                    bytes = new Gson().toJson(((InvocationTargetException) e).getTargetException()).getBytes();
                                } else {
                                    AbstractPrintTraceException abstractPrintTraceException =
                                            (AbstractPrintTraceException) e.getCause().getClass().getConstructors()[0].newInstance(returnedObject);
                                    abstractPrintTraceException.setDebugMessage(e.getCause().getMessage());
                                    bytes = new Gson().toJson(abstractPrintTraceException).getBytes();
                                }
                            } else {
                                bytes = WRONG_COMBINATION_EHANDLER_MESSAGE.getBytes();
                            }
                        } else {
                            bytes = buildResponseBody(
                                    method2.invoke(ClassUtils.getZeroParamsConstructor(
                                            controllers.get(endpointBo).getMethodDescription().get(currentVerb).getCertainClazz()).newInstance()))
                                    .toString().getBytes();
                        }
                    }
                    exchange.sendResponseHeaders(eHandler.status(), bytes.length);
                } catch (IllegalAccessException | InvocationTargetException | InstantiationException e1) {
                    bytes = buildResponseBody(onErrorInvoke(currentObj, endpointBo, currentVerb)).toString().getBytes();
                    exchange.sendResponseHeaders(501, bytes.length);
                }
                break;
            }
        }
        if (!isCustomExceptionHandlingPresent) {
            bytes = buildResponseBody(onErrorInvoke(currentObj, endpointBo, currentVerb)).toString().getBytes();
            exchange.sendResponseHeaders(501, bytes.length);
        }
        return bytes;
    }
}
