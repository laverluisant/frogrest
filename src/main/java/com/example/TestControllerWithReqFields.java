package com.example;

import http.server.annotation.InBodyParamsMap;
import http.server.annotation.Param;
import http.server.annotation.crud.*;

import java.util.Map;

/**
 * Example of controller restricted by required field which give back map of request params.
 * Using for test from {@link com.example.TestControllerWithReqFieldsTest}.
 * <p>
 * Created by laverluisant on 29.05.17.
 */
@CRUDController(endpoint = "/test/test2", isCustomizable = true)
public class TestControllerWithReqFields {

    @Get
    @RequiredFields(fields = "test")
    public Object get(@InBodyParamsMap Map inBody) {
        return inBody;
    }

    @Post
    @RequiredFields(fields = "test")
    public Object post(@InBodyParamsMap Map inBody) {
        return inBody;
    }

    @Put
    @RequiredFields(fields = "test")
    public Object put(@InBodyParamsMap Map inBody) {
        return inBody;
    }

    @Delete
    @RequiredFields(fields = "test")
    public Object delete(@InBodyParamsMap Map inBody) {
        return inBody;
    }

    @CustomPoint(verb = "TEST")
    public Object custom(
            @Param(required = true, name = "test") String test) {
        return test;
    }
}
