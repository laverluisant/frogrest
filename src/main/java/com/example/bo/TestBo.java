package com.example.bo;

import java.io.Serializable;

/**
 * Created by laverluisant on 25.05.17.
 *
 * Test bo. Using in {@link com.example.TestControllerPathVariables} and {@link com.example.TestControllerInBodyParams}
 * for testing serialization and deserialization of custom bo.
 */
public class TestBo implements Serializable {
    private String text;

    public TestBo(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
