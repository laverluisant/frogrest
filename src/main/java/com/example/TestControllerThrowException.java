package com.example;

import com.example.exception.TestException;
import com.example.exception.TestExceptionNotChildAbstractPrintException;
import com.example.exception.TestExceptionNotChildAbstractPrintException2;
import com.example.exception.TestExcetion2;
import com.sun.net.httpserver.Headers;
import http.server.annotation.*;
import http.server.annotation.crud.*;

import java.util.Map;

/**
 * Simple controller to demonstrate how @{@link ExceptionHandler} and {@link Default} can be used
 * with {@link CustomPoint} and {@link CRUDController} annotations.
 * Using for test from {@link com.example.TestControllerThrowExceptionTest}.
 */
@CRUDController(endpoint = "/path/test/test/test", isCustomizable = true)
public class TestControllerThrowException {

    public final static String CUSTOM_EXCEPTION_MSG = String.format("%s greets you!", TestControllerThrowException.class);
    public final static String METHOD_EXCEPTION_MSG = "METHOD greets you!";
    public final static String ON_ERROR_DEFAULT_MSG = "OnError default handling greets you!";


    @Get
    public Object get(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        throw new TestException(METHOD_EXCEPTION_MSG);
    }


    @Post
    public Object post(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        throw new TestExceptionNotChildAbstractPrintException(METHOD_EXCEPTION_MSG);
    }


    @Default
    @Put
    public Object put(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        throw new TestExceptionNotChildAbstractPrintException2(METHOD_EXCEPTION_MSG);
    }


    @Delete
    public Object delete(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws TestExcetion2 {
        throw new TestExcetion2(METHOD_EXCEPTION_MSG);
    }

    @ExceptionHandler(exceptions = {TestException.class}, status = 400, isPrintStackTrace = true)
    public String backError() {
        return CUSTOM_EXCEPTION_MSG;
    }

    @ExceptionHandler(exceptions = {TestExceptionNotChildAbstractPrintException.class}, status = 444)
    public String backError2() {
        return CUSTOM_EXCEPTION_MSG;
    }

    @ExceptionHandler(
            exceptions = {TestExceptionNotChildAbstractPrintException2.class},
            status = 410,
            isPrintStackTrace = true)
    public String backError3() {
        return CUSTOM_EXCEPTION_MSG;
    }

    @ExceptionHandler(
            exceptions = {TestExcetion2.class},
            status = 416,
            isPrintStackTrace = false)
    public String backError4() {
        return CUSTOM_EXCEPTION_MSG;
    }

    @CustomPoint(verb = "ONERROR")
    public void customPointOnError() throws Exception {
        throw new Exception("Default onError handling.");
    }

    @OnError
    public String onError() {
        return ON_ERROR_DEFAULT_MSG;
    }
}
