package com.example;

import com.sun.net.httpserver.Headers;
import http.server.annotation.*;
import http.server.annotation.rest.RestController;
import http.server.annotation.rest.RestPoint;
import http.server.util.BaseUtils;
import http.server.handler.impl.RestEndpointHandlerEnum;

/**
 * Simple controller to demonstrate {@link http.server.annotation.crud.CRUDController}.
 * Using for test from {@link com.example.TestRestControllerSimpleTest}.
 */
@RestController(endpoint = "t/")
public class TestRestControllerSimple {

    @Default
    @RestPoint(endpoint = "t1/t2/t3/{t4}/", verb = RestEndpointHandlerEnum.Constants.GET)
    public void get() {
    }

    @Default
    @RestPoint(endpoint = "t1/t2/t3/{t4}/{t5}", verb = RestEndpointHandlerEnum.Constants.GET)
    @ResponseHeaders(headers = {
            @ResponseHeader(name = "Host", value = "localhost:8765"),
            @ResponseHeader(name = "Accept-language", value = "en-Us")})
    public String get2(
            @PathVariable(name = "t4") String t4,
            @PathVariable(name = "t5") String t5,
            @Param(name = "t6") String t6,
            Headers requestHeaders) {
        return t4 + BaseUtils.AMPERSAND + t5 + BaseUtils.AMPERSAND + t6;
    }

    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/", verb = RestEndpointHandlerEnum.Constants.POST)
    public void post(@PathVariable(name = "t3") String t) {
    }

    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/{t5}", verb = RestEndpointHandlerEnum.Constants.POST)
    public String post2(@PathVariable(name = "t3") String t3,
                        @Param(name = "t6") String t6,
                        @Param(name = "t7") int t7) {
        return t3 + BaseUtils.AMPERSAND + t6 + BaseUtils.AMPERSAND + t7;
    }

    @RestPoint(endpoint = "t1/t2/t3/{t4}/", verb = RestEndpointHandlerEnum.Constants.PUT)
    public void put() {
    }

    @RestPoint(endpoint = "t1/t2/t3/{t4}/{t5}", verb = RestEndpointHandlerEnum.Constants.PUT)
    public String put2(
            @PathVariable(name = "t4") String t4,
            @PathVariable(name = "t5") String t5,
            @Param(name = "t6") String t6) {
        return t4 + BaseUtils.AMPERSAND + t5 + BaseUtils.AMPERSAND + t6;
    }

    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/", verb = RestEndpointHandlerEnum.Constants.DELETE)
    public void delete(@PathVariable(name = "t3") String t) {
    }

    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/{t5}", verb = RestEndpointHandlerEnum.Constants.DELETE)
    public String delete2(@PathVariable(name = "t3") String t3,
                          @Param(name = "t6") String t6,
                          @Param(name = "t7") int t7) {
        return t3 + BaseUtils.AMPERSAND + t6 + BaseUtils.AMPERSAND + t7;
    }

    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/", verb = "CUSTOM")
    public void custom(@PathVariable(name = "t3") String t) {
    }

    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/{t5}", verb = "CUSTOM")
    public String custom2(@PathVariable(name = "t3") String t3,
                          @Param(name = "t6") String t6,
                          @Param(name = "t7") int t7) {
        return t3 + BaseUtils.AMPERSAND + t6 + BaseUtils.AMPERSAND + t7;
    }
}
