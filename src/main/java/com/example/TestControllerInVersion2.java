package com.example;

import http.server.annotation.Version.Version;
import http.server.annotation.rest.RestController;
import http.server.annotation.rest.RestPoint;
import http.server.handler.impl.RestEndpointHandlerEnum;

@Version(id = 2)
@RestController(endpoint = "versions")
public class TestControllerInVersion2 {
    @RestPoint(verb = RestEndpointHandlerEnum.Constants.GET)
    public String get() {
        return "version 2";
    }

    @RestPoint(verb = RestEndpointHandlerEnum.Constants.POST)
    public String post() {
        return "version 2";
    }

    @RestPoint(verb = RestEndpointHandlerEnum.Constants.PUT)
    public String put() {
        return "version 2";
    }

    @RestPoint(verb = RestEndpointHandlerEnum.Constants.DELETE)
    public String delete() {
        return "version 2";
    }

    @RestPoint(verb = "CUSTOM")
    public String custom() {
        return "version 2";
    }
}
