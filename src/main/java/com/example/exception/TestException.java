package com.example.exception;

import http.server.exception.AbstractPrintTraceException;

/**
 * Exception to demonstrate how works handling E extend {@link AbstractPrintTraceException}.
 * Using in {@link com.example.TestControllerThrowException}.
 */
public class TestException extends AbstractPrintTraceException {
    public TestException(String message) {
        super(message);
    }
}
