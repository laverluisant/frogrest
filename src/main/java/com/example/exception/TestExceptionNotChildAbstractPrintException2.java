package com.example.exception;

/**
 * Exception to demonstrate how works handling E not extend {@link http.server.exception.AbstractPrintTraceException}.
 * Using in {@link com.example.TestControllerThrowException}. The same as {@link TestExceptionNotChildAbstractPrintException}
 * but using to show another point of behavior.
 */
public class TestExceptionNotChildAbstractPrintException2 extends Exception {
    public TestExceptionNotChildAbstractPrintException2(String message) {
        super(message);
    }
}
