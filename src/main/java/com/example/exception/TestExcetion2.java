package com.example.exception;

import http.server.exception.AbstractPrintTraceException;

/**
 * Exception to demonstrate how works handling E extend {@link AbstractPrintTraceException}.
 * Using in {@link com.example.TestControllerThrowException}. The same as {@link TestException}
 * but using to show another point of behavior.
 */
public class TestExcetion2 extends AbstractPrintTraceException {
    public TestExcetion2(String message) {
        super(message);
    }
}
