package com.example.exception;

/**
 * Exception to demonstrate how works handling E not extend {@link http.server.exception.AbstractPrintTraceException}.
 * Using in {@link com.example.TestControllerThrowException}.
 */
public class TestExceptionNotChildAbstractPrintException extends Exception {

    public TestExceptionNotChildAbstractPrintException(String message) {
        super(message);
    }
}
