package com.example;

import http.server.server.HTTPServerStarter;
import http.server.server.ServerStarter;

import java.util.Arrays;

/**
 * Simple runner.
 */
public class Application {

    public static void main(String[] args) {
        ServerStarter serverStarter = new HTTPServerStarter(Arrays.asList("com/example"));
        serverStarter.start();
    }
}
