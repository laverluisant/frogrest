package com.example;

import com.example.bo.TestBo;
import com.sun.net.httpserver.Headers;
import http.server.annotation.*;
import http.server.annotation.crud.*;

import java.util.Map;

/**
 * Example of rest GET controller which give back map of inbody params.
 * Add custom verbs "FIRST" and "SECOND" endpoint.
 * Using for test from {@link com.example.TestControllerInBodyParamsTest}.
 * <p>
 * Created by laverluisant on 06.11.17.
 */
@CRUDController(endpoint = "/path/{test2}/{test3}/param", isCustomizable = true)
public class TestControllerInBodyParams {

    @Before
    public void before() {
        System.out.println("Before greets you.");
    }

    @After
    public void after() {
        System.out.println("After buys you.");
    }

    @Get
    public Object get(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @Post
    public Object post(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @Put
    public Object put(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @Default
    @Delete
    public Object delete(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @CustomPoint(verb = "SECOND")
    public String test(
            @PathVariable(name = "test2") String test2,
            @Param(name = "test") String test3,
            @PathVariable(name = "test3") String s2,
            @Param(name = "text", required = true) TestBo test) {
        return s2;
    }

    @CustomPoint(verb = "FIRST")
    public String first(@PathVariable(name = "test2") String test2) {
        return test2;
    }

    @CustomPoint(verb = "ONERROR")
    public void customPointOnError() throws Exception {
        throw new Exception("Default onError handling.");
    }
}
