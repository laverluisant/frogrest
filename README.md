#### **FRogRest** is simple http server with the primary or most-commonly-used HTTP verbs like POST, GET, PUT and DELETE as default method and allows to use custom verb endpoint listeners. These correspond to create, read, update, and delete (or CRUD) operations, respectively. ####

## Quick start  ##

#### Create server ####
```
#!java

ServerStarter serverStarter = new HTTPServerStarter(packages of controllers, port, threads) // port, threads are optional, default 8765 and single thread;

```
#### Start server ####
```
#!java
serverStarter.start();

```

#### Stop server ####
```
#!java
serverStarter.stop();

```
#### Example ####
```
#!java

ServerStarter serverStarter = new HTTPServerStarter(Arrays.asList("example/test"), 8080, 1);
serverStarter.start();
...
serverStarter.stop();

```

## Create controller ##

#### Can be used two types of controller: ####
* `@CRUDController` is crud controller which mandatory contains get, post, put and delete points and can contain custom point methods.
* `@RestController` is classic rest controller. Can contain multi-endpoint `@RestPoint` methods. 

#### `@CRUDController` and `@RestController` allow for implementation classes to be autodetected through classpath scanning as controller. It obligatorily contains four default CRUD methods: Get, Post, Put and Delete, and can contains custom methods. ####

#### To create a base CRUD controller: ####
```
#!java
@CRUDController(endpoint = "/path/{test2}/{test3}/param", isCustomizeble = true)
public class TestControllerInBodyParams {

    @Get
    public Object get(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @Post
    public Object post(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @Put
    public Object put(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }

    @Default
    @Delete
    public Object delete(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }
}

```
#### Custom point for crud definition. ####
```
#!java
@CRUDController(endpoint = "/path/{test2}/{test3}/param", isCustomizeble = true)
public class TestControllerInBodyParams {
...

    @CustomPoint(verb = "SECOND")
    public String test(
            @PathVariable(name = "test2") String test2,
            @Param(name = "test3") String test3,
            @PathVariable(name = "test5") String s2,
            @Param(name = "text", required = true) TestBo test) {
        return s2;
    }
```

#### To create a multi endpoint controller: ####
#### To create rest endpoint method, need annotate class `@RestController` and method as `@RestPoint`. ####
```
#!java
@RestController(endpoint = "t/")
public class TestRestControllerSimple {

    @Default
    @RestPoint(endpoint = "t1/t2/t3/{t4}/", verb = RestEndpointHandlerEnum.Constants.GET)
    public void get() {
    }
    ...
}
```
#### `@Default` means OPTIONS access. Should be unique in class terms or throws DuplicationDefaultAnnotationRuntimeException. The OPTIONS method represents a request for information about the communication options available on the request/response chain identified by the Request-URI. This method allows the client to determine the options and/or requirements associated with a resource, or the capabilities of a server, without implying a resource action or initiating a resource retrieval. ####
  
```
#!java

    @Default
    @Delete
    public Object delete(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return inBody;
    }
``` 
#### Required parameters declaration ####
#### To declare required parameters can be used two approach ####
* `@RequiredFields`  to define the name list of required parameters which should exist in Map annotated `@InBodyParamsMap`.

```
#!java

@CRUDController(endpoint = "/test/test2", isCustomizable = true)
public class TestControllerWithReqFields {

    @Get
    @RequiredFields(fields = "test")
    public Object get(@InBodyParamsMap Map inBody) {
        return inBody;
    }
```
* `@Param(required = true,...)` to define current parameter as required.
```
#!java
    @CustomPoint(verb = "TEST")
    public Object custom(@Param(required = true, name = "test") String test) {
        return test;
    }
```

#### Can be used as default rest methods (GET, POST, PUT, DELETE) as custom. To determine default methods can be used constants from RestEndpointHandlerEnum.Constants. ####
#### Examples of custom verb method: ####
```
#!java
@RestController(endpoint = "t/")
public class TestRestControllerSimple {
...
    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/", verb = "CUSTOM")
    public void custom(@PathVariable(name = "t3") String t) {
    }
...
}
```
```
#!java
@CRUDController(endpoint = "/path/{test2}/{test3}", isCustomizable = true)
public class TestControllerPathVariables {

    @CustomPoint(verb = "SECOND")
    public String test(
            @PathVariable(name = "test2") String test2,
            @Param(name = "test") String test3,
            @PathVariable(name = "test3") String s2,
            @Param(name = "text", required = true) TestBo test) {
        return s2;
    }
    ...
    
}
```

## Path variable ##
#### To bind requests to controller methods via request pattern. Take a request like http://domain/path/213234 and you can easily bind it to a controller method via annotation and bind path variables. And get map of variable as second income params in controller methods. ####
```
#!java
@RestController(endpoint = "t/{param}")
public class TestRestControllerSimple {
...
    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/", verb = "CUSTOM")
    public void custom(@PathVariable(name = "t3") String t) {
    }
...
}
```
```
#!java
@CRUDController(endpoint = "/path/{test2}/{test3}", isCustomizable = true)
public class TestControllerPathVariables {

    @Get
    public Object get(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return pathVariable;
    }
    ...
    
}
```
## Request body parameters: ##
#### To parse income body parameters use `@Param` to annotated method income variable ####
```
#!java
    @RestPoint(endpoint = "{t1}/t2/{t3}/t4/{t5}", verb = "CUSTOM")
    public String custom2(@PathVariable(name = "t3") String t3,
                          @Param(name = "t6") String t6,
                          @Param(name = "t7") int t7) {
        return t3 + BaseUtils.AMPERSAND + t6 + BaseUtils.AMPERSAND + t7;
    }

```
## Parameters Maps: ##
* `@InBodyParamsMap` contains all params from request body. For GET method it contains parsed url after ?.
* `@PathVariablesMap` contains all params from request url.
```
#!java
    @Get
    public Object get(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, ...) {
        return pathVariable;
    }

```
## Headers: ##
* Request headers can be passed to point method via method income variable which type is `Header`. Can exist only one variable this type for one point method.
```
#!java
    @Post
    public Object post(@InBodyParamsMap Map inBody, @PathVariablesMap Map pathVariable, Headers headers) throws Exception {
        return pathVariable;
    }
```
* Response headers are create by `@ResponseHeaders` and `@ResponseHeader` on point method.
```
#!java
    @RestPoint(endpoint = "t1/t2/t3/{t4}/{t5}", verb = RestEndpointHandlerEnum.Constants.GET)
    @ResponseHeaders(headers = {
            @ResponseHeader(name = "Host", value = "localhost:8765"),
            @ResponseHeader(name = "Accept-language", value = "en-Us")})
    public String get2(
            @PathVariable(name = "t4") String t4,
            @PathVariable(name = "t5") String t5,
            @Param(name = "t6") String t6,
            Headers requestHeaders) {
        return t4 + BaseUtils.AMPERSAND + t5 + BaseUtils.AMPERSAND + t6;
    }
```

## Versions ##
#### Annotation for handling multi-version approach in endpoint building. To use version dividing need to add `@Vesion(id = 1, word = "version")` to controller class. ####
* Id is mandatory field and sets version number. 
* Word is optional field which would be used together with id to build rest endpoint. If no word were set would be used default value as 'v'.

```
#!java
@Version(id = 1)
@RestController(endpoint = "versions")
public class TestControllerInVersion1 {
```
#### To achieve this controller should be user - 'v1/versions' url. ####

## Exceptions ##
#### Annotation for handling exceptions in specific handler methods. Handler methods which are annotated with this annotation are allowed to have very flexible signatures. Method can have return type like Object or Subtype or be void. Every exception can be annotated only once in controller or it'll be back DuplicateExceptionAnnotationRuntimeException. ####

```
#!java

@ExceptionHandler(exceptions = {SomeException1.class}, status = 400)
    public Object backException() {...}
    
@ExceptionHandler(exceptions = {SomeException2.class}, status = 501)
    public void voidExeption() {...}
```
#### To response to client stack trace of Exception, use `@ExceptionHandler(isPrintStackTrace = true)` and thrown Exception should be child of AbstractPrintTraceException. ####
#### By default, BaseUtils.class has onError() method with behavior for `ComingSoonException | CannotConvertResponseException | IllegalAccessException | InvocationTargetException`. For overriding it in controller use `@OnError` annotation.  ####
#### Method to be annotated `@OnError` should have no income parameters and can be void or return any object. If method has any return value it would be sent to client else would be send error request with empty body. In both case return code would be 501. `@OnError` annotated method can't be `static`. ####
```
#!java
    @OnError
    public Object onError() {
        return "Something goes wrong way!";
    }
```
#### To send to client exception stack traces, thorwn exception should be clild of `AbstractPrintTraceException` and `@ExceptionHandler(... isPrintStackTrace = true)`. If thown exception type isn't a child when would be back response with message = "Wrong combination annotation @ExceptionHandler with param isPrintStackTrace = true and not E extend AbstractPrintTraceException!" ####

```
#!java
    @ExceptionHandler(
            exceptions = {TestExceptionNotChildAbstractPrintException2.class},
            status = 410,
            isPrintStackTrace = true)
    public String backError3() {
        return CUSTOM_EXCEPTION_MSG;
    }
```

## Before and after invoke point method handling ##
#### Any controller can have non static, void and has no income parameters annotated as `@Before` and `@After`  ####
```
#!java
    @Before
    public void before() {
        System.out.println("Before greets you.");
    }

    @After
    public void after() {
        System.out.println("After buys you.");
    }
```

## Let's write test
#### To create test for `@CRUDController` or `@RestController` annotated class need use Junit and mark class as `@Case`. It allows reach controller by url-key. Now Controller.class and ControllerTest.class should be allocated in same package. Future it would be change.####
#### Create Test class:
#### ControllerTest.class should contains static method annotated `@BeforeClass` starting MockServer in and be annotated `@Case` with url key(default url = '/'). Inside static method annotated `@BeforeClass`(junit) should be initiated MockServerStarted.####
```
#!java

@Case(url = "path/test2/3")
public class TestControllerPathVariablesTest {

    private static MockHTTPServerStarter serverStarter;

    @BeforeClass
    public static void runBeforeClass() {
        serverStarter = new MockHTTPServerStarter();
        serverStarter.start();
    }

    @AfterClass
    public static void runAfterClass() {
        serverStarter.stop();
    }
```
#### Create test:
#### Test is method annotated `@Test`(junit) and @MethodCase to set necessary params for Mock server. `@MethodCase(testDataNameReferences = {"..."}` defines references to `@TestData(name = "...")` to determine which data should be set to this case. To send a request to test server use `Object obj = serverStarter.call()`. ####

```
#!java

    @Test
    @MethodCase(
            name = "TEST 1. GET",
            description = "Test for GET METHOD throws exception processed in method @ExceptionHandler." +
                    " E extend AbstractPrintTraceException.",
            testDataNameReferences = {"get"})
    public void testGET() {
        Object obj = serverStarter.call();
        ...
        }
```
#### Create test data:####
#### TestData type determines from TestDataTypeEnum.Constants variable and define which type of parser should be use.####
```
#!java
    @TestData(type = TestDataTypeEnum.Constants.MAP, name = "post")
    public static HashMap<String, String> post = new HashMap<>() {{
        put("post", "post");
        put("post1", (new ArrayList<String>() {{
            add("post2");
            add("post3");
        }}).toString());
    }};
```